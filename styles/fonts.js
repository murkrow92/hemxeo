export const Arial = 'Arial';
export const HelveticaNeueBold = 'HelveticaNeue-Bold';

export const OPEN_SANS = 'Open Sans';
export const ICIEL_AUTHER = 'Iciel Auther';
export const ICIEL_CADENA = 'Iciel Cadena';
export const HELVETICA_NEUE = 'HelveticaNeue';
