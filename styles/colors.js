export const COLOR_FACEBOOK_BOLD_BLUE = '#3b5998';

export const COLOR_PURPLE = '#7373FA';
export const COLOR_WHITE = '#FFFFFF';
export const COLOR_GREY_AE = '#AEAEAE';
export const COLOR_GREY_F7 = '#F7F7F7';
export const COLOR_GREY_F2 = '#F2F2F2';
export const COLOR_GREY_BACKGROUND = '#BDC3C7';
export const COLOR_GREY_E4 = '#E4E4E4';
export const COLOR_GREY_D7 = '#D7D7D7';
export const COLOR_DARK_33 = '#333333';
export const COLOR_DARK_66 = '#666666';
export const COLOR_GREY_99 = '#999999';

export const COLOR_SIGN_FIRE = '#F05A5B';
export const COLOR_SIGN_WATER = '#23A7EF';
export const COLOR_SIGN_AIR = '#F6D76B';
export const COLOR_SIGN_EARTH = '#87D37B';

export const COLOR_BLUE = '#63A3FE';
export const COLOR_RED = '#FF0000';

export const COLOR_BLACK = '#000000';
export const COLOR_GREEN = '#008000';

export const COLOR_LIGHT_BLACK = '#101319';
export const COLOR_YELLOW = '#f3cd83';

export const COLOR_BACKGROUND_BLUE = '#080D40';
export const COLOR_BUTTON_BLUE = '#1A2D89';
export const COLOR_BUTTON_MIDDLE_BLUE = '#466BB2';
export const COLOR_BUTTON_LIGHT_BLUE = '#6DA3D7';

export const COLOR_PANEL_BACKGROUND_BLUE = '#243F99';
export const COLOR_LIGHT_BLUE = '#4A90E2';

export const COLORS_GRADIENT_BLUE = [
    COLOR_BUTTON_BLUE,
    COLOR_BUTTON_MIDDLE_BLUE,
    COLOR_BUTTON_LIGHT_BLUE
];

export const COLORS_GRADIENT_BLUE_UP = [
    COLOR_BUTTON_LIGHT_BLUE,
    COLOR_BUTTON_MIDDLE_BLUE,
    COLOR_BUTTON_BLUE
];

export const COLORS_GRADIENT_BUTTON = [
    COLOR_PANEL_BACKGROUND_BLUE,
    '#16256B',
    COLOR_BACKGROUND_BLUE
];

export const COLOR_DESCRIPTION_GRAY = '#9B9B9B';
