import React from 'react';
import { StyleSheet } from 'react-native';
import { COLOR_BLACK, COLOR_WHITE } from './colors';

const styleApp = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR_BLACK
    },
    title: {
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: 'HelveticaNeue'
    },
    pageContainer: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center'
    },
    content: {
        color: COLOR_WHITE,
        fontSize: 14,
        fontFamily: 'HelveticaNeue'
    }
});

export default styleApp;
