import React from 'react';
import { StyleSheet } from 'react-native';
import { COLOR_BACKGROUND_BLUE, COLOR_WHITE } from './colors';
import { HELVETICA_NEUE, ICIEL_CADENA, OPEN_SANS } from './fonts';

const styleApp = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR_BACKGROUND_BLUE
    },
    appTitle: {
        color: COLOR_WHITE,
        fontSize: 38,
        fontWeight: 'normal',
        fontFamily: 'Iciel Auther'
    },
    loading: {
        color: COLOR_WHITE,
        fontSize: 18,
        fontWeight: 'normal',
        fontFamily: 'Iciel Cadena'
    },
    buttonContainer: {
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonLabel: {
        color: COLOR_WHITE,
        fontSize: 16,
        fontWeight: 'normal',
        fontFamily: ICIEL_CADENA
    },
    title: {
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: '100',
        fontFamily: OPEN_SANS
    },
    subTitle: {
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: '100',
        fontFamily: 'Open Sans',
        marginTop: -5
    },
    pageContainer: {
        paddingHorizontal: 15
    },
    content: {
        color: COLOR_WHITE,
        fontSize: 14,
        fontFamily: HELVETICA_NEUE
    },
    avatar: {
        width: 32,
        height: 32,
        borderRadius: 16
    }
});

export default styleApp;
