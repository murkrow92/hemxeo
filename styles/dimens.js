import { Platform, StatusBar } from 'react-native';

export const ICON_SIZE_20 = 20;
export const ICON_SIZE_24 = 24;
export const ICON_SIZE_14 = 14;

export const STATUS_BAR_HEIGHT =
    Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
