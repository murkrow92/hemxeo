import {
    COLOR_BLACK,
    COLOR_DARK_33,
    COLOR_GREY_99,
    COLOR_RED,
    COLOR_WHITE
} from './colors';
import { StyleSheet } from 'react-native';

export default (styleApp = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR_WHITE,
        alignItems: 'flex-start',
        justifyContent: 'flex-start'
    },
    center: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    chevronRightContainer: {
        paddingRight: 10,
        justifyContent: 'center',
        flex: 1,
        alignItems: 'flex-end'
    },
    directionColumnPadding10: {
        flexDirection: 'column',
        paddingVertical: 10
    },
    directionRow: {
        flexDirection: 'row',
        backgroundColor: '#FFFFFF'
    },
    itemRowPadding10: {
        paddingVertical: 10,
        width: '100%',
        flexDirection: 'row'
    },
    itemRowPadding15: {
        paddingVertical: 10,
        paddingHorizontal: 15,
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center'
    },
    areaPadding10: {
        padding: 10
    },
    openSans14Dark: {
        fontFamily: 'OpenSans',
        color: COLOR_BLACK,
        fontSize: 14
    },
    openSans14Dark33: {
        fontFamily: 'OpenSans',
        color: COLOR_DARK_33,
        fontSize: 14
    },
    openSans14Dark33Bold: {
        fontFamily: 'OpenSans',
        color: COLOR_DARK_33,
        fontSize: 14,
        fontWeight: 'bold'
    },
    openSans13Dark33: {
        fontFamily: 'OpenSans',
        color: COLOR_DARK_33,
        fontSize: 13
    },
    openSans12Grey: {
        fontFamily: 'OpenSans',
        color: COLOR_GREY_99,
        fontSize: 12
    },
    openSans12GreyBold: {
        fontFamily: 'OpenSans',
        color: COLOR_GREY_99,
        fontSize: 12,
        fontWeight: 'bold'
    },
    openSans13Red: {
        fontFamily: 'OpenSans',
        color: COLOR_RED,
        fontSize: 13
    },
    openSans16Dark: {
        fontFamily: 'OpenSans',
        color: COLOR_BLACK,
        fontSize: 16
    },
    openSans16Dark33: {
        fontFamily: 'OpenSans',
        color: COLOR_DARK_33,
        fontSize: 16
    },
    openSans18Dark33: {
        fontFamily: 'OpenSans',
        color: COLOR_DARK_33,
        fontSize: 18
    },
    openSans18Red: {
        fontFamily: 'OpenSans',
        color: COLOR_RED,
        fontSize: 18
    },
    arialWhite15: {
        fontFamily: 'Arial',
        color: COLOR_WHITE,
        fontSize: 15
    },
    arialWhite18: {
        fontFamily: 'Arial',
        color: COLOR_WHITE,
        fontSize: 18
    },
    helvetica13Dark33: {
        fontFamily: 'Helvetica',
        color: COLOR_DARK_33,
        fontSize: 13
    },
    helvetica33Dark33: {
        fontFamily: 'Helvetica',
        color: COLOR_DARK_33,
        fontSize: 33
    },
    avatar: {
        width: 32,
        height: 32,
        borderRadius: 16
    }
}));
