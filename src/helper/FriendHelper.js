import lodash from 'lodash';
import StringHelper from './StringHelper';

const MAX_ITEM_PER_PAGE = 10;

export default class FriendHelper {

    static getRandom10Friends(friends) {
        let tmp = [];
        let result = [];
        lodash.forEach(friends, (v, k) => {
            tmp.push(v);
        });
        if (tmp.length > 10) {
            while (result.length < 10) {
                const r = Math.floor(Math.random() * tmp.length);
                result.push(tmp[r]);
                tmp.splice(r, 1);
            }
            return result;
        } else {
            return tmp;
        }
    }

    static getFacebookFriends(friends, searchText = '', page = 1) {
        const text = StringHelper.replaceVietnamese(searchText);
        const ls = [];
        lodash.forEach(friends, function(value, key) {
            const name = StringHelper.replaceVietnamese(value.name);
            if (text === '' || name.includes(text)) {
                ls.push(value);
            }
        });
        ls.reverse();
        const limit = MAX_ITEM_PER_PAGE * page;
        if (ls.length > limit) {
            return ls.slice(0, limit);
        }
        return ls;
    }

    static getNewFacebookFriends(vnastroFriends, facebookFriends) {
        let result = [];
        lodash.forEach(facebookFriends, function(value, key) {
            const facebookId = value.id;
            let existed = false;
            lodash.forEach(vnastroFriends, function(v, k) {
                if (v.fb_id == facebookId) {
                    existed = true;
                }
            });
            if (!existed) {
                value.fb_id = value.id;
                result.push(value);
            }
        });

        return result;
    }
}
