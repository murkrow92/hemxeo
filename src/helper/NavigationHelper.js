class NavigationHelper {
    static gotoAstro(navigate, profile, astro) {
        const { id } = profile.data.data;
        const { done } = astro.data.daily;
        if (id > 0) {
            if (done == 1) {
                navigate('AstroDetail');
            } else {
                navigate('MainAstro');
            }
        } else {
            navigate('Login');
        }
    }

    static gotoTarot(navigate, tarot) {
        const { done } = tarot.data.daily;
        if (done == 1) {
            navigate('TarotDetail');
        } else {
            navigate('MainTarot');
        }
    }
}

export default NavigationHelper;
