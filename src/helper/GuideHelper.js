class GuideHelper {
    static standard(messages = [], user = {}) {
        let result = [];
        for (let i = 0; i < messages.length; i++) {
            let content = messages[i];
            let showAvatar = i == 0;
            let lastChild = i == messages.length - 1;
            result.push({
                content: content,
                showAvatar: showAvatar,
                lastChild: lastChild,
                avatar: { uri: user.avatar[80] }
            });
        }
        return result;
    }
}

export default GuideHelper;
