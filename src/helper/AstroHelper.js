import lodash from 'lodash';

import ARIES from '../../assets/images/signs/1.png';
import TAURUS from '../../assets/images/signs/2.png';
import GEMINI from '../../assets/images/signs/3.png';
import CANCER from '../../assets/images/signs/4.png';
import LEO from '../../assets/images/signs/5.png';
import VIRGO from '../../assets/images/signs/6.png';
import LIBRA from '../../assets/images/signs/7.png';
import SCORPIO from '../../assets/images/signs/8.png';
import SAGITTARIUS from '../../assets/images/signs/9.png';
import CAPRICORN from '../../assets/images/signs/10.png';
import AQUARIUS from '../../assets/images/signs/11.png';
import PISCES from '../../assets/images/signs/12.png';

import ASCENDANT from '../../assets/images/planet/AC.png';
import MIDHEAVEN from '../../assets/images/planet/mc.png';
import SUN from '../../assets/images/planet/sun.png';
import PURPLEAIR from '../../assets/images/planet/purple_air.png';
import MOON_2 from '../../assets/images/planet/Moon_2.png';
import MERCURY from '../../assets/images/planet/mercury.png';
import VENUS from '../../assets/images/planet/venus.png';
import MARS from '../../assets/images/planet/mars.png';
import JUPITER from '../../assets/images/planet/jupiter.png';
import SATURN from '../../assets/images/planet/saturn.png';
import URANUS from '../../assets/images/planet/uranus.png';
import NEPTUNE from '../../assets/images/planet/neptune.png';
import PLUTO from '../../assets/images/planet/pluto.png';
import NORTH_NODE from '../../assets/images/planet/lahau.png';
import SOUTH_NODE from '../../assets/images/planet/kedo.png';
import CHIRON from '../../assets/images/planet/chiron.png';
import VERTEX from '../../assets/images/planet/vertex.png';
import LILITH from '../../assets/images/planet/lilith.png';

import {
    COLOR_SIGN_AIR,
    COLOR_SIGN_EARTH,
    COLOR_SIGN_FIRE,
    COLOR_SIGN_WATER
} from '../../styles/colors';

export const signImages = [
    ARIES,
    TAURUS,
    GEMINI,
    CANCER,
    LEO,
    VIRGO,
    LIBRA,
    SCORPIO,
    SAGITTARIUS,
    CAPRICORN,
    AQUARIUS,
    PISCES
];

export const planets = [
    ASCENDANT,
    SUN,
    MOON_2,
    PURPLEAIR,
    MERCURY,
    VENUS,
    MARS,
    JUPITER,
    SATURN,
    URANUS,
    NEPTUNE,
    PLUTO,
    MIDHEAVEN,
    NORTH_NODE,
    SOUTH_NODE,
    CHIRON,
    VERTEX,
    LILITH
];

const objNames = [
    'Ascendant',
    'Sun',
    'Moon',
    'PurpleAir',
    'Mercury',
    'Venus',
    'Mars',
    'Jupiter',
    'Saturn',
    'Uranus',
    'Neptune',
    'Pluto',
    'MC',
    'NorthNode',
    'SouthNode',
    'Chiron',
    'Vertex',
    'Lilith'
];

const objPosition = [
    { house: 1 },
    { planet: 1 },
    { planet: 2 },
    { planet: 16 },
    { planet: 3 },
    { planet: 4 },
    { planet: 5 },
    { planet: 6 },
    { planet: 7 },
    { planet: 8 },
    { planet: 9 },
    { planet: 10 },
    { house: 10 },
    { planet: 13 },
    { planet: 14 },
    { planet: 11 },
    { planet: 34 },
    { planet: 12 }
];

const viObjNames = [
    'AC',
    'Mặt trời',
    'Mặt trăng',
    'Tử Khí',
    'Thuỷ tinh',
    'Kim tinh',
    'Hoả tinh',
    'Mộc tinh',
    'Thổ tinh',
    'Thiên Vương tinh',
    'Hải Vương tinh',
    'Diêm Vương tinh',
    'MC',
    'La Hầu',
    'Kế Đô',
    'Chiron',
    'Vertex',
    'Lilith'
];

const MAX_ITEM_PER_PAGE = 10;

export default class AstroHelper {
    static getAstroObjList(data, page = 1) {
        let result = [];
        objNames.map(function(value, index) {
            if (data.Planets[value]) {
                result.push({
                    ...data.Planets[value],
                    name: value,
                    key: value,
                    type: 'planet'
                });
            }
        });

        lodash.forEach(data.Houses, function(value, key) {
            if (value.house !== 1 && value.house !== 10) {
                let house = {
                    ...value,
                    name: key,
                    key: key,
                    type: 'house'
                };
                result.push(house);
            }
        });

        const limit = MAX_ITEM_PER_PAGE * page;
        if (result.length > limit) {
            return result.slice(0, limit);
        }

        return result;
    }

    static getSignImage(sign) {
        return signImages[sign - 1];
    }

    static getViPlanetName(name) {
        return viObjNames[objNames.indexOf(name)];
    }

    static getObjectPositionByName(name) {
        return objPosition[objNames.indexOf(name)];
    }

    static getPlanetImageByName(name) {
        return AstroHelper.getPlanetImage(objNames.indexOf(name) + 1);
    }

    static getPlanetImage(planet) {
        return planets[planet - 1];
    }

    static getHouseBackgroundColor(house) {
        const odd = house % 4;

        if (odd == 1) {
            return COLOR_SIGN_FIRE;
        } else if (odd == 2) {
            return COLOR_SIGN_EARTH;
        } else if (odd == 3) {
            return COLOR_SIGN_AIR;
        } else {
            return COLOR_SIGN_WATER;
        }
    }

    static getDaily(article, avatar) {
        let result = [];
        let { description, passage } = article;

        let pieces = description.trim().split('. ');

        for (let i = 0; i < pieces.length; i++) {
            result.push({
                content: pieces[i].trim(),
                showAvatar: i == 0,
                lastChild: i == pieces.length - 1,
                avatar: avatar
            });
        }

        for (let k in passage) {
            let p = passage[k];
            result.push({
                content: p.title,
                showAvatar: true,
                lastChild: false,
                avatar: avatar
            });

            let pieces = p.content.split('. ');
            for (let i = 0; i < pieces.length; i++) {
                result.push({
                    content: pieces[i].trim(),
                    showAvatar: false,
                    lastChild: i == pieces.length - 1,
                    avatar: avatar
                });
            }
        }

        return result;
    }
}
