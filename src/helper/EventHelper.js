import lodash from 'lodash';
import StringHelper from './StringHelper';

export default class EventHelper {
    static toArray(events) {
        let result = [];
        lodash.forEach(events, (v, k) => {
            result.push(v);
        });
        result.sort((e1, e2) => {
            return e1.start_date - e2.start_date;
        });
        return result;
    }

    static shortenEventTitle(title = '') {
        const t = StringHelper.replaceVietnamese(title);
        return t.includes('tarot') ? 'Tarot' : 'Astro';
    }
}
