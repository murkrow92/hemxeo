import lodash from 'lodash';


export default class ArticleHelper {
    static toArray(articles) {
        let result = [];
        lodash.forEach(articles, (v, k) => {
            result.push(v);
        });
        return result;
    }

}
