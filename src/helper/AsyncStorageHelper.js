import { AsyncStorage } from 'react-native';

const KEY_FACEBOOK_PROFILE = 'com.keyfacebook.profile';
const KEY_PROFILE = 'key.user.profile';
const KEY_ASTRO = 'com.key.astro';
const KEY_FRIENDS = 'com.key.friends';
const KEY_CITY = 'com.key.city';
const KEY_BASE = 'com.key.base';
const KEY_ARTICLE = 'com.key.timeline';
const KEY_COMBO_MEAN = 'com.key.combo.mean';
const KEY_VNASTRO_TOKEN = 'com.key.vnastro.token';
const KEY_READER = 'com.key.reader';
const KEY_EVENT = 'com.key.event';
const KEY_PRODUCT = 'com.key.product';
const KEY_FEEDBACK = 'com.key.feedback';
const KEY_BANK = 'com.key.bank';
const KEY_CONFIG = 'con.key.config';
const KEY_DAILY_TAROT = 'com.key.daily.tarot';
const KEY_DAILY_ASTRO = 'com.key.daily.astro';

export default class AsyncStorageHelper {
    static saveVnastroToken(response) {
        AsyncStorage.setItem(KEY_VNASTRO_TOKEN, JSON.stringify(response));
    }

    static getVnastroToken() {
        return AsyncStorage.getItem(KEY_VNASTRO_TOKEN);
    }

    static saveFacebookProfile(response) {
        AsyncStorage.setItem(KEY_FACEBOOK_PROFILE, JSON.stringify(response));
    }

    static getFacebookProfile() {
        return AsyncStorage.getItem(KEY_FACEBOOK_PROFILE);
    }

    static saveAstro(response, key_time) {
        AsyncStorage.setItem(KEY_ASTRO + key_time, JSON.stringify(response));
    }

    static getAstro(key_time) {
        return AsyncStorage.getItem(KEY_ASTRO + key_time);
    }

    static removeAstro(key_time) {
        AsyncStorage.removeItem(KEY_ASTRO + key_time);
    }

    static saveUserProfile(data) {
        AsyncStorage.setItem(KEY_PROFILE, JSON.stringify(data));
    }

    static getUserProfile() {
        return AsyncStorage.getItem(KEY_PROFILE);
    }

    static removeUserProfile() {
        return AsyncStorage.removeItem(KEY_PROFILE);
    }

    static saveFriends(data) {
        AsyncStorage.setItem(KEY_FRIENDS, JSON.stringify(data));
    }

    static getFriends() {
        return AsyncStorage.getItem(KEY_FRIENDS);
    }

    static saveCity(data) {
        AsyncStorage.setItem(KEY_CITY, JSON.stringify(data));
    }

    static getCity() {
        return AsyncStorage.getItem(KEY_CITY);
    }

    static saveBase(data) {
        AsyncStorage.setItem(KEY_BASE, JSON.stringify(data));
    }

    static getBase() {
        return AsyncStorage.getItem(KEY_BASE);
    }

    static saveArticles(data) {
        AsyncStorage.setItem(KEY_ARTICLE, JSON.stringify(data));
    }

    static getArticles() {
        return AsyncStorage.getItem(KEY_ARTICLE);
    }

    static saveComboMean(comboString, data) {
        AsyncStorage.setItem(
            KEY_COMBO_MEAN + comboString,
            JSON.stringify(data)
        );
    }

    static getComboMean(comboString) {
        return AsyncStorage.getItem(KEY_COMBO_MEAN + comboString);
    }

    static saveReader(data) {
        AsyncStorage.setItem(KEY_READER, JSON.stringify(data));
    }

    static getReader() {
        return AsyncStorage.getItem(KEY_READER);
    }

    static saveEvent(data) {
        AsyncStorage.setItem(KEY_EVENT, JSON.stringify(data));
    }

    static getEvent() {
        return AsyncStorage.getItem(KEY_EVENT);
    }

    static saveProduct(data) {
        AsyncStorage.setItem(KEY_PRODUCT, JSON.stringify(data));
    }

    static getProduct() {
        return AsyncStorage.getItem(KEY_PRODUCT);
    }

    static saveFeedback(data) {
        AsyncStorage.setItem(KEY_FEEDBACK, JSON.stringify(data));
    }

    static getFeedback() {
        return AsyncStorage.getItem(KEY_FEEDBACK);
    }

    static saveBank(data) {
        AsyncStorage.setItem(KEY_BANK, JSON.stringify(data));
    }

    static getBank() {
        return AsyncStorage.getItem(KEY_BANK);
    }

    static saveConfig(data) {
        AsyncStorage.setItem(KEY_CONFIG, JSON.stringify(data));
    }

    static getConfig() {
        return AsyncStorage.getItem(KEY_CONFIG);
    }

    static saveTarot(data, dateFormat) {
        AsyncStorage.setItem(
            KEY_DAILY_TAROT + dateFormat,
            JSON.stringify(data)
        );
    }

    static getTarot(dateFormat) {
        return AsyncStorage.getItem(KEY_DAILY_TAROT + dateFormat);
    }

    static saveDailyAstro(data, dateFormat) {
        AsyncStorage.setItem(
            KEY_DAILY_ASTRO + dateFormat,
            JSON.stringify(data)
        );
    }

    static getDailyAstro(dateFormat) {
        return AsyncStorage.getItem(KEY_DAILY_ASTRO + dateFormat);
    }
}
