export default class WordHelper {
    static censorName(text) {
        let result = '';
        if (text) {
            result = text;
            let arr = text.split(' ');
            if (arr.length > 2) {
                result = arr[0] + ' ' + arr[1];

                for (let i = 2; i < arr.length; i++) {
                    let tmp = '';
                    for (let j = 0; j < arr[i].length; j++) {
                        tmp = tmp + '*';
                    }
                    result = result + ' ' + tmp;
                }
            }
            return result;
        }
    }

    static cutName(text) {
        let result = '';
        if (text) {
            let arr = text.split(' ');
            result = arr[arr.length - 1];
            if (arr.length > 1) {
                result = arr[arr.length - 2] + ' ' + result;
            }
        }
        return result;
    }
}
