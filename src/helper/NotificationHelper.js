import OneSignal from 'react-native-onesignal';
import { ONE_SINGNAL_APP_ID } from '../../config';
import { Platform } from 'react-native';

class NotificationHelper {
    static register = () => {
        OneSignal.init(ONE_SINGNAL_APP_ID, { kOSSettingsKeyAutoPrompt: true });
        OneSignal.inFocusDisplaying(2);
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('ids', this.onIds);
    };

    static unregister = () => {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('ids', this.onIds);
    };

    static onReceived = notification => {
        console.log('Notification received: ', notification);
    };

    static onOpened = openResult => {
        console.log('Message: ', openResult.notification.payload.body);
        console.log('Data: ', openResult.notification.payload.additionalData);
        console.log('isActive: ', openResult.notification.isAppInFocus);
        console.log('openResult: ', openResult);
    };

    static onIds = device => {
        console.log('Device info: ', device);
    };

    static checkStatus = callback => {
        if (Platform.OS === 'ios') {
            OneSignal.checkPermissions(permissions => {
                if (permissions.alert) {
                    OneSignal.getPermissionSubscriptionState(status => {
                        callback(status.userSubscriptionEnabled);
                    });
                } else {
                    callback(false);
                }
            });
        } else {
            OneSignal.getPermissionSubscriptionState(status => {
                callback(status.userSubscriptionEnabled);
            });
        }
    };

    static subscribe() {
        if (Platform.OS === 'ios') {
            OneSignal.checkPermissions(permissions => {
                if (permissions.alert) {
                    OneSignal.requestPermissions({
                        alert: true,
                        badge: true,
                        sound: true
                    });
                }
            });
        }
        OneSignal.setSubscription(true);
    }
}

export default NotificationHelper;
