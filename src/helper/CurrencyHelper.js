import currencyFormatter from 'currency-formatter';

export default class CurrencyHelper {
    static format(number) {
        return currencyFormatter.format(number, {
            locale: 'vi-VN',
            precision: 0
        });
    }
}
