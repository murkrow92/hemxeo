import lodash from 'lodash';

export default class TarotHelper {
    static standardlize(passages, description) {
        let result = [];
        result.push({ title: 'Miêu tả', content: description });
        lodash.forEach(passages, (v, k) => {
            result.push(v);
        });
        return result;
    }

    static standardlizeHistory(history) {
        let result = [];
        lodash.forEach(history, (v, k) => {
            v.day = k;
            result.push(v);
        });
        return result;
    }
}
