import { ShareDialog } from 'react-native-fbsdk';

const SAMPLE_CONTENT = {
    contentType: 'photo',
    imageUrl: '',
    caption: '',
    userGenerated: true
};

class ShareHelper {
    static share(content = SAMPLE_CONTENT) {
        console.log(content);
        ShareDialog.canShow(content)
            .then(canShow => {
                if (canShow) {
                    return ShareDialog.show(content);
                }
            })
            .then(
                result => {
                    console.log('share result: ', result);
                    if (result.isCancelled) {
                        alert('Đã hủy');
                    } else {
                        alert('Đã chia sẻ thành công');
                    }
                },
                error => {
                    alert('Share fail with error: ' + error);
                }
            );
    }
}

export default ShareHelper;
