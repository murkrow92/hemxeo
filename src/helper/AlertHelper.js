import { Alert } from 'react-native';

export default class AlertHelper {
    static alertUpdated(
        callback = () => {
            console.log('OK Pressed');
        }
    ) {
        if (!callback) {
            callback = () => console.log('OK Pressed');
        }

        Alert.alert(
            'Thông báo',
            'Đã cập nhật',
            [{ text: 'OK', onPress: callback }],
            { cancelable: false }
        );
    }

    static alertBookSuccess(callback) {
        if (!callback) {
            callback = () => console.log('OK Pressed');
        }

        Alert.alert(
            'Thông báo',
            'Đã đặt thành công',
            [{ text: 'OK', onPress: callback }],
            { cancelable: false }
        );
    }

    static alertError(callback, error = 'Đã có lỗi xảy ra') {
        if (!callback) {
            callback = () => console.log('OK Pressed');
        }
        Alert.alert('Lỗi', error, [{ text: 'OK', onPress: callback }], {
            cancelable: false
        });
    }
}
