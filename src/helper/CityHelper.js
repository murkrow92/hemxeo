import lodash from 'lodash';
import StringHelper from './StringHelper';

export default class CityHelper {
    static getList(city) {
        const sorted = lodash.orderBy(city.data, 'title', 'asc');
        let result = [];
        result.push({
            id: '22',
            title: 'Hà Nội'
        });
        result.push({
            id: '29',
            title: 'Tp Hồ Chí Minh'
        });
        lodash.forEach(sorted, function(value, key) {
            if (value.id != '22' && value.id != '29') {
                result.push(value);
            }
        });
        return result;
    }

    static getCityById(cityList, cityId) {
        let result = { title: '' };
        lodash.forEach(cityList, function(value, key) {
            if (value.id == cityId) {
                result = value;
            }
        });
        return result;
    }

    static filter(city, searchText) {
        const text = StringHelper.replaceVietnamese(searchText);
        let result = [];
        const lsCity = this.getList(city);
        lodash.forEach(lsCity, function(value, key) {
            const name = StringHelper.replaceVietnamese(value.title);
            if (text === '' || name.includes(text)) {
                value.key = value.id;
                result.push(value);
            }
        });
        return result;
    }
}
