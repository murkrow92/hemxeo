import queryString from 'query-string';
import moment from 'moment';
import CityHelper from './CityHelper';

export default class StringHelper {
    static replaceVietnamese(text) {
        return text
            .toLowerCase()
            .replace(' ', '')
            .replace(/[àáạảãâầấậẩẫăằắặẳẵ]/g, 'a')
            .replace(/[èéẹẻẽêềếệểễ]/g, 'e')
            .replace(/[ìíịỉĩ]/g, 'i')
            .replace(/[òóôõöộổ]/g, 'o')
            .replace(/[ùúụủũưừứựửữ]/g, 'u')
            .replace(/[ỳýỵỷỹ]/g, 'y')
            .replace(/đ/g, 'd');
    }

    static shortenTitle(text) {
        if (text.length > 60) {
        }
    }

    static buildAstroQueryString(profile) {
        const now = new Date();
        const { day, month, year, hour, minute, city_id } = profile;
        let query = queryString.stringify({
            year: year || now.getFullYear(),
            month: month || now.getMonth(),
            day: day || now.getDay(),
            hour: hour || now.getHours(),
            minute: minute || now.getMinutes(),
            city_id: city_id
        });
        return query;
    }

    static buildAstroDisplayString(profile, cities) {
        const {
            day,
            month,
            year,
            hour,
            minute,
            city_id,
            fullname,
            name,
            bday,
            bhour,
            bmonth,
            bminute,
            byear
        } = profile;

        const date = moment(
            `${day || bday}-${month || bmonth}-${year || byear} ${hour ||
                bhour}:${minute || bminute}`,
            'DD-MM-YYYY HH:mm'
        ).format('DD/MM/YYYY HH:mm');
        const cityName = CityHelper.getCityById(cities, city_id).title;
        const n = fullname || name;
        return n + ', ' + date + ', ' + cityName;
    }
}

export const DEGREE_SYMBOL = '\xB0';
