import lodash from 'lodash';

export default class FeedbackHelper {
    static get2RandomFeedbacks(data) {
        let tmpResult = [];
        let result = [];
        lodash.forEach(data, (v, k) => {
            tmpResult.push(v);
        });

        if (tmpResult.length >= 2) {
            let r1 = Math.floor(Math.random() * tmpResult.length);
            result.push(tmpResult[r1]);
            tmpResult.splice(r1, 1);
            let r2 = Math.floor(Math.random() * tmpResult.length);
            result.push(tmpResult[r2]);
            return result;
        } else {
            return tmpResult;
        }
    }
}
