import lodash from 'lodash';
import StringHelper from './StringHelper';

export default class ProductHelper {
    static getTarot(data) {
        let result = {};
        lodash.forEach(data, (v, k) => {
            const title = StringHelper.replaceVietnamese(v.title);
            if (title.includes(StringHelper.replaceVietnamese('Đọc tarot'))) {
                result = v;
            }
        });
        return result;
    }

    static getAstro(data) {
        let result = {};
        lodash.forEach(data, (v, k) => {
            const title = StringHelper.replaceVietnamese(v.title);
            const astroTitle = StringHelper.replaceVietnamese('Đọc chiêm tinh');
            if (title.includes(astroTitle)) {
                result = v;
            }
        });
        return result;
    }

    static get(id, data) {
        let result = {};
        lodash.forEach(data, (v, k) => {
            if (v.id == id) {
                result = v;
            }
        });
        return result;
    }

    static filterProductName(text) {
        return text.toLowerCase().includes('tarot') ? 'TAROT' : 'ASTRO';
    }

    static getBankTransferContent(event, order){
        const productName = ProductHelper.filterProductName(
            event.product.title
        );
        const hash = order.hash.slice(0, 2);
        const phone = order.phone.slice(
            order.phone.length - 3,
            order.phone.length - 1
        );

        return productName + ' ' + phone + hash;
    }
}
