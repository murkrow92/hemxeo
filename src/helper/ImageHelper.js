import BlankProfile from '../../assets/images/blank_profile.png';
import { VNASTRO_URL } from '../../config';

export default class ImageHelper {
    static getPictureFromFacebookObject(facebookId, pictureObject = {}) {
        let picture = BlankProfile;
        if (typeof pictureObject === 'object' && facebookId !== '') {
            picture = {
                uri: `https://graph.facebook.com/${facebookId}/picture?type=large`
            };
        }
        return picture;
    }

    static getPictureFromPageUrl(url) {
        return url === '' ? BlankProfile : { uri: url };
    }

    static getAvatarFromServer(url) {
        return url
            ? {
                  uri: VNASTRO_URL + '/' + url
              }
            : BlankProfile;
    }
}
