import lodash from 'lodash';

export default class ReaderHelper {
    static getReaders(readers) {
        let result = [];
        lodash.forEach(readers, (v, k) => {
            result.push(v);
        });
        return result;
    }

    static getServiceReader(product_id, readers) {
        let result = [];
        lodash.forEach(readers, (v, k) => {
            lodash.forEach(v.time, (v1, k1) => {
                if (v1.product_id == product_id) {
                    if (!result.includes(v)) {
                        result.push(v);
                    }
                }
            });
        });

        if (result.length > 0) {
            result.push({
                id: 'all',
                user: {
                    fullname: 'Tất cả'
                }
            });
        }

        result.reverse();

        return result;
    }
}
