import { StyleSheet } from 'react-native';
import { COLOR_WHITE } from '../../../styles/colors';

const styles = StyleSheet.create({
    pageContainer: {
        flex: 3,
        paddingHorizontal: 15,
        paddingTop: 25
    },
    loginContainer: {
        paddingHorizontal: 50,
        paddingVertical: 50,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        width: 70,
        height: 68
    },
    buttonContainer: {
        marginTop: 40,
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        paddingHorizontal: 10,
        textAlign: 'center',
        color: COLOR_WHITE,
        fontSize: 16,
        fontWeight: 'normal',
        fontFamily: 'Iciel Cadena'
    }
});

export default styles;
