import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as facebookActions from '../../redux/actions/FacebookActions';
import * as profileActions from '../../redux/actions/ProfileActions';

import styleApp from '../../../styles/stylesv3';

import styles from '../LoginScreenv2/styles';
import CustomStatusBar from '../../components/CustomStatusBar';
import Cover from '../../components/Cover';
import {
    COLOR_BLACK,
    COLORS_GRADIENT_BLUE,
    COLORS_GRADIENT_BUTTON
} from '../../../styles/colors';
import LinearGradient from 'react-native-linear-gradient';

import Logo from '../../../assets/images/logo.png';

class LoginScreen extends React.Component {
    render() {
        return (
            <View style={styleApp.container}>
                <CustomStatusBar height={0} />
                <Cover />
                <View style={styles.pageContainer}>
                    <LinearGradient
                        start={{ x: 0.5, y: 0 }}
                        end={{ x: 0.5, y: 1 }}
                        shadowColor={COLOR_BLACK}
                        shadowOffset={{ width: 0, height: 0 }}
                        shadowOpacity={60}
                        shadowRadius={5}
                        colors={COLORS_GRADIENT_BUTTON}
                        style={styles.loginContainer}
                    >
                        <Image source={Logo} style={styles.logo} />
                        <TouchableOpacity onPress={this.signIn}>
                            <LinearGradient
                                start={{ x: 0, y: 0 }}
                                end={{ x: 1, y: 1 }}
                                colors={COLORS_GRADIENT_BLUE}
                                style={styles.buttonContainer}
                            >
                                <Text style={styles.label}>
                                    Đăng nhâp Facebook
                                </Text>
                            </LinearGradient>
                        </TouchableOpacity>
                    </LinearGradient>
                </View>
            </View>
        );
    }

    signIn = () => {
        const { facebookActions } = this.props;
        facebookActions.signIn();
    };

    componentDidMount() {}

    componentDidUpdate(prevProps, prevState) {
        const { navigate } = this.props.navigation;
        const { profileActions } = this.props;
        const { facebook, profile } = this.props;
        const prevFacebook = prevProps.facebook;
        const prevProfile = prevProps.profile;
        const { id } = profile.data.data;
        const prevId = prevProfile.data.data.id;

        if (id != prevId) {
            navigate('Home');
        } else if (facebook.data.id != prevFacebook.data.id) {
            const { email, id } = facebook.data;
            profileActions.signIn(email, id);
        }
    }
}

const mapStateToProps = state => ({
    facebook: state.facebook,
    profile: state.profile
});

const mapDispatchToProps = dispatch => ({
    facebookActions: bindActionCreators(facebookActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
