import { StyleSheet } from 'react-native';
import {
    COLOR_FACEBOOK_BOLD_BLUE,
    COLOR_GREY_BACKGROUND,
    COLOR_WHITE
} from '../../../styles/colors';
import { HelveticaNeueBold } from '../../../styles/fonts';

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        paddingBottom: 50,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 10
    }
});

export default styles;
