import React from 'react';
import { View, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileActions from '../../redux/actions/ProfileActions';
import * as cityActions from '../../redux/actions/CityActions';

import CustomStatusBar from '../../components/CustomStatusBar';
import styleApp from '../../../styles/stylesv3';
import { PureCover } from '../../components/Cover';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import NavigationBar from '../../components/NavigationBar';
import BottomButton from '../../components/BottomButton';
import Conversation from '../../components/Conversation';
import CustomDateTimePicker from '../../components/DateTimePicker';
import CityPicker from '../../components/CityPicker';
import GuideHelper from '../../helper/GuideHelper';
import Divider from '../../components/Divider';
import CityHelper from '../../helper/CityHelper';

class MainAstroScreen extends React.Component {
    render() {
        const { main } = this.props.config.data.data.astro;
        const { day, month, year } = this.props.profile.data.data;
        return (
            <View style={styleApp.container}>
                <CustomStatusBar backgroundColor="transparent" height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
                {day > 0 && month > 0 && year > 0 ? (
                    <BottomButton
                        label="Tiếp tục"
                        handler={this.gotoAstroDetail}
                    />
                ) : (
                    <BottomButton label={main.button} handler={this.handler} />
                )}
            </View>
        );
    }

    renderContent = () => {
        const { city } = this.props;
        const { main } = this.props.config.data.data.astro;
        const { guide, user } = main;
        const g1 = GuideHelper.standard(guide[0], user);
        const g2 = GuideHelper.standard(guide[1], user);
        const {
            day,
            month,
            year,
            hour,
            minute,
            city_id
        } = this.props.profile.data.data;
        const cityList = CityHelper.getList(city);

        return (
            <View style={[styleApp.pageContainer, { marginTop: -50 }]}>
                <Conversation data={g1} />
                <Divider height={20} color="transparent" />
                <Conversation data={g2} />
                {day > 0 && month > 0 && year > 0 ? (
                    <View />
                ) : (
                    <View>
                        <CustomDateTimePicker
                            ref={picker => (this.datePicker = picker)}
                            day={day}
                            month={month}
                            year={year}
                            hour={hour}
                            minute={minute}
                        />
                        <CityPicker
                            ref={picker => (this.cityPicker = picker)}
                            cityList={cityList}
                            selectedCityId={city_id}
                        />
                    </View>
                )}
            </View>
        );
    };

    renderNavBar = () => {
        const { goBack } = this.props.navigation;
        return <NavigationBar title="Trang chủ" goBack={goBack} />;
    };

    renderToolBar = () => {
        const { goBack } = this.props.navigation;
        return <PureCover goBack={goBack} />;
    };

    handler = () => {
        const { profileActions } = this.props;
        const { data } = this.props.profile.data;
        const { date } = this.datePicker.state;
        const { selectedCityId } = this.cityPicker.state;

        const updated = {
            ...data,
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
            hour: date.getHours(),
            minute: date.getMinutes(),
            city_id: selectedCityId
        };

        profileActions.update(updated);
    };

    gotoAstroDetail = () => {
        const { navigate } = this.props.navigation;
        navigate('AstroDetail');
    };

    componentDidMount() {
        const { city } = this.props;
        console.log(city);
    }

    componentDidUpdate(prevProps, prevState) {
        const { navigate } = this.props.navigation;
        const { updated } = this.props.profile.data;
        const prevUpdated = prevProps.profile.data.updated;

        if (updated && !prevUpdated) {
            navigate('AstroDetail');
        }
    }
}

const mapStateToProps = state => ({
    config: state.config,
    profile: state.profile,
    facebook: state.facebook,
    city: state.city
});

const mapDispatchToProps = dispatch => ({
    profileActions: bindActionCreators(profileActions, dispatch),
    cityActions: bindActionCreators(cityActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MainAstroScreen);
