import React from 'react';

import {
    StyleSheet,
    View,
    Dimensions,
    Image,
    Text,
    TouchableOpacity
} from 'react-native';
import Carousel from 'react-native-snap-carousel';
import PropTypes from 'prop-types';

import TarotBackground from '../../../assets/images/tarotbg.png';
import { COLOR_LIGHT_BLUE } from '../../../styles/colors';
import { ICIEL_CADENA } from '../../../styles/fonts';

class Deck extends React.Component {
    constructor(props) {
        super(props);
        const { length } = this.props.data;
        const firstIndex = Math.round(Math.random() * (length - 30)) + 20;
        this.state = {
            firstIndex: firstIndex
        };
    }

    renderItem = ({ item, index }) => {
        const { onItemPress } = this.props;
        return (
            <View style={styles.itemContainer}>
                <TouchableOpacity onPress={onItemPress}>
                    <Image source={TarotBackground} style={styles.card} />
                </TouchableOpacity>
                <Text style={styles.label}>{item}</Text>
            </View>
        );
    };

    onSnapToItem = index => {
        this.setState({
            currentIndex: index
        });
    };

    render() {
        const { width } = Dimensions.get('window');
        const { firstIndex } = this.state;

        return (
            <View style={styles.container}>
                <Carousel
                    ref={c => {
                        this.carousel = c;
                    }}
                    enableSnap={false}
                    inactiveSlideScale={0.8}
                    inactiveSlideOpacity={0.6}
                    loop={true}
                    firstItem={firstIndex}
                    data={this.props.data}
                    renderItem={this.renderItem}
                    sliderWidth={width}
                    itemWidth={width * 0.25}
                    onSnapToItem={this.onSnapToItem}
                />
            </View>
        );
    }

    componentDidUpdate() {
        if (this.callback) {
            setTimeout(() => {
                this.callback();
                this.callback = null;
            }, 500);
        }
    }

    snapToItem = (index, callback = () => {}) => {
        this.carousel.snapToItem(index, true);
        this.callback = callback;
    };
}

const styles = StyleSheet.create({
    container: {
        paddingVertical: 20
    },
    itemContainer: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    card: {
        width: 90,
        height: 137
    },
    label: {
        textAlign: 'center',
        color: COLOR_LIGHT_BLUE,
        fontSize: 16,
        fontWeight: 'normal',
        fontFamily: ICIEL_CADENA
    }
});

export default Deck;
