import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    pageContainer: {
        flex: 1,
        paddingBottom: 50,
        justifyContent: 'flex-end',
        alignItems: 'center',
        paddingHorizontal: 10
    }
});

export default styles;
