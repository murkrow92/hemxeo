import React from 'react';
import { View, Platform } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import CustomStatusBar from '../../components/CustomStatusBar';
import styleApp from '../../../styles/stylesv3';
import { PureCover } from '../../components/Cover';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import NavigationBar from '../../components/NavigationBar';
import BottomButton from '../../components/BottomButton';
import Conversation from '../../components/Conversation';
import Deck from './Deck';
import GuideHelper from '../../helper/GuideHelper';
import * as tarotActions from '../../redux/actions/TarotActions';

let data = [];
for (let i = 1; i <= 78; i++) {
    data.push(i);
}

class MainTarotScreen extends React.Component {
    render() {
        const { main } = this.props.config.data.data.tarot;

        return (
            <View style={styleApp.container}>
                <CustomStatusBar backgroundColor="transparent" height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
                <BottomButton label={main.button} handler={this.handler} />
            </View>
        );
    }

    renderContent = () => {
        const { main } = this.props.config.data.data.tarot;
        const { guide, user } = main;
        const m1 = GuideHelper.standard(guide[0], user);
        const m2 = GuideHelper.standard(guide[1], user);

        return (
            <View style={[styleApp.pageContainer, { marginTop: -50 }]}>
                <Conversation data={m1} />
                <Deck
                    ref={deck => (this.deck = deck)}
                    onItemPress={this.gotoDetail}
                    data={data}
                />
                <Conversation data={m2} />
            </View>
        );
    };

    renderNavBar = () => {
        const { goBack } = this.props.navigation;
        return <NavigationBar title="Trang chủ" goBack={goBack} />;
    };

    renderToolBar = () => {
        const { goBack } = this.props.navigation;
        return <PureCover goBack={goBack} />;
    };

    componentDidMount() {
        // console.log('Tarot:', this.props.config.data.data.tarot);
    }

    componentDidUpdate(prevProps, prevState) {
        const { done } = this.props.tarot.data.daily;
        const prevDone = prevProps.tarot.data.daily.done;
        const { navigate } = this.props.navigation;

        if (prevDone == 0 && done > 0) {
            navigate('TarotDetail');
        }
    }

    handler = () => {
        const r = Math.floor(Math.random() * data.length);
        this.deck.snapToItem(r, this.gotoDetail);
    };

    gotoDetail = () => {
        const { tarotActions, tarot } = this.props;
        const { navigate } = this.props.navigation;
        const { done } = tarot.data.daily;

        if (done == 1) {
            navigate('TarotDetail');
        } else {
            tarotActions.drawDailyCard();
        }
    };
}

const mapStateToProps = state => ({
    facebook: state.facebook,
    config: state.config,
    profile: state.profile,
    tarot: state.tarot
});

const mapDispatchToProps = dispatch => ({
    tarotActions: bindActionCreators(tarotActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MainTarotScreen);
