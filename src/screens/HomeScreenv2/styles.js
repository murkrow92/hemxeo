import { StyleSheet } from 'react-native';
import { COLOR_WHITE } from '../../../styles/colors';

const styles = StyleSheet.create({

    pageContainer: {
        paddingHorizontal: 15,
        flex: 3,
        paddingTop: 80,
        flexDirection: 'row'
    },
    images: {
        width: '100%',
        height: 250,
        justifyContent: 'flex-end'
    }
});

export default styles;
