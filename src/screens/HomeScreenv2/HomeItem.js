import React from 'react';
import LinearGradient from 'react-native-linear-gradient';

import {
    ImageBackground,
    Text,
    TouchableOpacity,
    StyleSheet,
    View,
    Image,
    Dimensions
} from 'react-native';
import styleApp from '../../../styles/stylesv3';
import {
    COLOR_DESCRIPTION_GRAY,
    COLORS_GRADIENT_BLUE,
    COLORS_GRADIENT_BLUE_UP
} from '../../../styles/colors';
import { OPEN_SANS } from '../../../styles/fonts';

import TarotBackground from '../../../assets/images/tarotbg.png';
import AstroBackground from '../../../assets/images/astrobg.png';

const { height, width } = Dimensions.get('window');
console.log('height: ', height, ' width: ', width);

const HomeItem = ({
    index,
    background,
    label,
    description,
    onPress,
    onPressCaption,
    astroImage
}) => {
    const colors =
        index % 2 == 0 ? COLORS_GRADIENT_BLUE : COLORS_GRADIENT_BLUE_UP;
    const defBg = index % 2 == 0 ? TarotBackground : AstroBackground;
    const containerStyle =
        index % 2 == 0 ? styles.cardLeftContainer : styles.cardRightContainer;

    return (
        <View style={containerStyle}>
            <ImageBackground
                defaultSource={defBg}
                style={styles.images}
                resizeMode="cover"
                source={background}
            >
                {astroImage ? (
                    <View
                        style={{
                            alignItems: 'center',
                            justifyContent: 'center',
                            flex: 1
                        }}
                    >
                        <Image
                            style={[
                                styles.sign,
                                {
                                    width: width * 0.25,
                                    height: width * 0.25,
                                    borderRadius: width * 0.125
                                }
                            ]}
                            source={astroImage}
                        />
                    </View>
                ) : (
                    <View />
                )}
                <TouchableOpacity
                    onPress={onPress}
                    style={{
                        borderBottomLeftRadius: 10,
                        borderBottomRightRadius: 10
                    }}
                >
                    <LinearGradient
                        start={{ x: 0, y: 0 }}
                        end={{ x: 1, y: 1 }}
                        style={styles.buttonContainer}
                        colors={colors}
                    >
                        <Text style={styleApp.buttonLabel}>{label}</Text>
                    </LinearGradient>
                </TouchableOpacity>
            </ImageBackground>
            <TouchableOpacity onPress={onPressCaption}>
                <Text style={styles.description}>{description}</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    images: {
        overflow: 'hidden',
        borderRadius: 5,
        height: '60%',
        width: '100%',
        justifyContent: 'flex-end'
    },
    sign: {
        marginBottom: 10,
        alignSelf: 'center',
        width: 120,
        height: 120,
        borderRadius: 60
    },
    cardRightContainer: {
        width: '50%',
        alignItems: 'center',
        paddingLeft: 7.5
    },
    cardLeftContainer: {
        width: '50%',
        paddingRight: 7.5,
        alignItems: 'center'
    },
    buttonContainer: {
        paddingVertical: 10,
        alignItems: 'center',
        justifyContent: 'center'
    },
    description: {
        marginTop: 25,
        color: COLOR_DESCRIPTION_GRAY,
        fontSize: 14,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    }
});

export default HomeItem;
