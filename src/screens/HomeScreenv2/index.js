import React from 'react';
import { View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as facebookActions from '../../redux/actions/FacebookActions';

import styleApp from '../../../styles/stylesv3';
import styles from './styles';

import HomeItem from './HomeItem';
import CustomStatusBar from '../../components/CustomStatusBar';
import Cover from '../../components/Cover';
import NavigationHelper from '../../helper/NavigationHelper';
import AstroHelper from '../../helper/AstroHelper';
import ImageHelper from '../../helper/ImageHelper';

import TarotBackground from '../../../assets/images/tarotbg.png';
import AstroBackground from '../../../assets/images/astrobg.png';

class HomeScreen extends React.Component {
    render() {
        const { profile, facebook } = this.props;
        const { title, caption } = this.props.config.data.data;
        const items = this.props.config.data.data.home.items;
        const tarot = items[0];
        const astro = items[1];
        const astroCaption =
            profile.data.data.id > 0
                ? `Tiếp tục với ${facebook.data.name}`
                : astro.caption;
        const { id, picture } = facebook.data;
        const avatar = ImageHelper.getPictureFromFacebookObject(id, picture);
        return (
            <View style={styleApp.container}>
                <CustomStatusBar height={0} />
                <Cover
                    title={title}
                    subTitle={caption}
                    avatar={avatar}
                    onPressAvatar={this.gotoProfile}
                />
                <View style={styles.pageContainer}>
                    <HomeItem
                        onPress={this.gotoTarot}
                        index={0}
                        background={{
                            uri: this.getTarotImage() || TarotBackground
                        }}
                        label={tarot.title}
                        description={tarot.caption}
                        onPressCaption={this.gotoHistory}
                    />
                    <HomeItem
                        onPress={this.gotoAstro}
                        index={1}
                        background={AstroBackground}
                        label={astro.title}
                        description={astroCaption}
                        onPressCaption={() => {}}
                        astroImage={this.getAstroImage()}
                    />
                </View>
            </View>
        );
    }

    gotoTarot = () => {
        const { tarot } = this.props;
        const { navigate } = this.props.navigation;
        NavigationHelper.gotoTarot(navigate, tarot);
    };

    gotoAstro = () => {
        const { navigate } = this.props.navigation;
        const { profile, astro } = this.props;
        NavigationHelper.gotoAstro(navigate, profile, astro);
    };

    gotoProfile = () => {
        const { facebook } = this.props;
        const { id } = facebook.data;
        const { navigate } = this.props.navigation;
        if (id != '') {
            navigate('Profile');
        }
    };

    gotoHistory = () => {
        const { navigate } = this.props.navigation;
        navigate('History');
    };

    getTarotImage = () => {
        const { tarot } = this.props;
        const { card } = tarot.data.daily;
        if (card.images[80] != null) {
            return card.images[80];
        }
    };

    getAstroImage = () => {
        const { astro } = this.props.astro.data.daily;
        for (let k in astro) {
            if (astro[k].type == 2) {
                return AstroHelper.getSignImage(astro[k].position);
            }
        }
    };

    componentDidMount() {
        console.log('astro:', this.props.astro);
    }

    componentDidUpdate(prevProps, prevState) {}
}

const mapStateToProps = state => ({
    facebook: state.facebook,
    config: state.config,
    profile: state.profile,
    tarot: state.tarot,
    astro: state.astro
});

const mapDispatchToProps = dispatch => ({
    facebookActions: bindActionCreators(facebookActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
