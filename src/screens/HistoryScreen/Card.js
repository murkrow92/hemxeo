import React from 'react';
import TarotBackground from '../../../assets/images/tarotbg.png';

import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ShortButton } from '../../components/Button';
import {
    COLOR_DESCRIPTION_GRAY,
    COLOR_GREY_99,
    COLOR_WHITE
} from '../../../styles/colors';
import { OPEN_SANS } from '../../../styles/fonts';

const Card = ({
    day = '',
    reversed = false,
    image = 'https://vnastro.com/_img_server/2018/06/26/1529983047_5b31b047c0a1f.png',
    onPressImage
}) => {
    const transformDegree = reversed ? '180deg' : '0deg';

    return (
        <View style={styles.container}>
            <TouchableOpacity onPress={onPressImage}>
                <Image
                    resizeMode="contain"
                    source={{ uri: image }}
                    defaultSource={TarotBackground}
                    style={[
                        styles.card,
                        { transform: [{ rotate: transformDegree }] }
                    ]}
                />
            </TouchableOpacity>
            <Text style={styles.day}>{day}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 10
    },
    card: {
        height: 137,
        width: 90
    },
    day: {
        marginTop: 5,
        color: COLOR_GREY_99,
        fontSize: 14,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    }
});

export default Card;
