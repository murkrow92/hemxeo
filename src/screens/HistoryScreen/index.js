import React from 'react';
import { Platform, Text, View } from 'react-native';
import styleApp from '../../../styles/stylesv3';
import CustomStatusBar from '../../components/CustomStatusBar';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import ImageHelper from '../../helper/ImageHelper';
import NavigationBar from '../../components/NavigationBar';
import { ProfileCover } from '../../components/Cover';
import * as profileActions from '../../redux/actions/ProfileActions';
import * as tarotActions from '../../redux/actions/TarotActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import GridView from 'react-native-gridview';
import TarotHelper from '../../helper/TarotHelper';
import Card from './Card';

class HistoryScreen extends React.Component {
    render() {
        return (
            <View style={styleApp.container}>
                <CustomStatusBar color="transparent" height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
            </View>
        );
    }

    renderContent = () => {
        const { history } = this.props.tarot.data;
        const data = TarotHelper.standardlizeHistory(history);
        const dataSource = new GridView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        }).cloneWithRows(data);

        return (
            <GridView
                style={{ marginTop: 10, paddingHorizontal: 15 }}
                data={data}
                renderItem={this.renderHistoryItem}
                dataSource={null}
                itemsPerRow={3}
            />
        );
    };

    renderHistoryItem = (item, sectionID, rowID, itemIndex, itemID) => {
        const { reversed, card, day } = item;
        const image = card.images[80];

        return (
            <Card
                index={itemIndex}
                image={image}
                reversed={reversed}
                day={day}
                onPressImage={() => {
                    this.gotoTarotDetail(day);
                }}
            />
        );
    };

    renderNavBar = () => {
        const { goBack } = this.props.navigation;
        return <NavigationBar title="Trang chủ" goBack={goBack} />;
    };

    renderToolBar = () => {
        const { facebook } = this.props;
        const { id, picture } = facebook.data;
        const avatar = ImageHelper.getPictureFromFacebookObject(id, picture);

        return <ProfileCover fullname="Lịch sử rút bài" avatar={avatar} />;
    };

    gotoTarotDetail = day => {
        const { navigate } = this.props.navigation;
        navigate('TarotDetail', { day: day });
    };

    componentDidMount() {
        const { tarotActions } = this.props;
        tarotActions.fetchHistory();
    }
}

const mapStateToProps = state => ({
    profile: state.profile,
    facebook: state.facebook,
    tarot: state.tarot,
    city: state.city
});

const mapDispatchToProps = dispatch => ({
    profileActions: bindActionCreators(profileActions, dispatch),
    tarotActions: bindActionCreators(tarotActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen);
