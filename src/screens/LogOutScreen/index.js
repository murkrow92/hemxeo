import React from 'react';
import { Platform, View } from 'react-native';
import styleApp from '../../../styles/stylesv3';
import CustomStatusBar from '../../components/CustomStatusBar';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import NavigationBar from '../../components/NavigationBar';
import { PureCover } from '../../components/Cover';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileActions from '../../redux/actions/ProfileActions';
import * as facebookActions from '../../redux/actions/FacebookActions';
import { LoginButton, AccessToken } from 'react-native-fbsdk';

class LogOutScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styleApp.container}>
                <CustomStatusBar height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
            </View>
        );
    }

    renderContent = () => {
        const { facebook, profile } = this.props;
        const { id } = facebook.data;

        return id != '' ? (
            <View style={{ alignItems: 'center' }}>
                <LoginButton
                    publishPermissions={['publish_actions']}
                    onLoginFinished={(error, result) => {
                        if (error) {
                            alert('login has error: ' + result.error);
                        } else if (result.isCancelled) {
                            alert('login is cancelled.');
                        } else {
                            AccessToken.getCurrentAccessToken().then(data => {
                                alert(data.accessToken.toString());
                            });
                        }
                    }}
                    onLogoutFinished={this.logout}
                />
            </View>
        ) : (
            <View />
        );
    };

    renderNavBar = () => {
        const { goBack } = this.props.navigation;
        return <NavigationBar title="Trang chủ" goBack={goBack} />;
    };

    renderToolBar = () => {
        return <PureCover />;
    };

    logout = () => {
        const { profileActions, facebookActions } = this.props;
        const { navigate } = this.props.navigation;
        profileActions.logout();
        facebookActions.logout();
        navigate('Home');
    };
}

const mapStateToProps = state => ({
    profile: state.profile,
    facebook: state.facebook
});

const mapDispatchToProps = dispatch => ({
    profileActions: bindActionCreators(profileActions, dispatch),
    facebookActions: bindActionCreators(facebookActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(LogOutScreen);
