import React from 'react';
import { View, Platform, Text, Modal } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import CustomStatusBar from '../../components/CustomStatusBar';
import styleApp from '../../../styles/stylesv3';
import { PureCover } from '../../components/Cover';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import NavigationBar from '../../components/NavigationBar';
import BottomButton from '../../components/BottomButton';
import moment from 'moment';
import { COLOR_DESCRIPTION_GRAY } from '../../../styles/colors';
import CardBox from './CardBox';
import Divider from '../../components/Divider';
import * as tarotActions from '../../redux/actions/TarotActions';
import NavigationHelper from '../../helper/NavigationHelper';
import TarotHelper from '../../helper/TarotHelper';
import Passages from './Passages';
import AdBanner from '../../components/AdBanner';
import { ImageViewer } from 'react-native-image-zoom-viewer';
import ShareHelper from '../../helper/ShareHelper';
import NotificationHelper from '../../helper/NotificationHelper';

class TarotDetailScreen extends React.Component {
    constructor(props) {
        super(props);

        const day = this.isHistory()
            ? moment(
                  this.props.navigation.state.params.day,
                  'DD/MM/YYYY'
              ).toDate()
            : new Date();
        const key = this.isHistory()
            ? this.props.navigation.state.params.day
            : '';

        this.state = {
            modalVisible: false,
            day: day,
            key: key,
            remindButtonStatus: false
        };
    }

    render() {
        return (
            <View style={styleApp.container}>
                <CustomStatusBar backgroundColor="transparent" height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
                <AdBanner />
            </View>
        );
    }

    isHistory = () => {
        const { params } = this.props.navigation.state;
        return params && params.day;
    };

    renderContent = () => {
        const { day, key, remindButtonStatus } = this.state;
        const { modalVisible } = this.state;
        const { tarot } = this.props;
        const { title } = this.isHistory()
            ? tarot.data.history[key].card
            : tarot.data.daily.card;
        const image = this.isHistory()
            ? tarot.data.history[key].card.images[80]
            : tarot.data.daily.card.images[80];
        const { reversed } = this.isHistory()
            ? tarot.data.history[key]
            : tarot.data.daily;
        const { description, passage } = this.isHistory()
            ? tarot.data.history[key].article
            : tarot.data.daily.article;
        const passages = TarotHelper.standardlize(passage, description);

        const md = moment(day).format('DD');
        const mm = moment(day).format('MM');
        const my = moment(day).format('YYYY');

        return (
            <View style={[styleApp.pageContainer, { marginTop: -100 }]}>
                <Text
                    style={[styleApp.title, { color: COLOR_DESCRIPTION_GRAY }]}
                >
                    Ngày {md} tháng {mm} năm {my}
                </Text>
                <CardBox
                    onPressShare={this.share}
                    onPressImage={this.onZoom}
                    image={image}
                    cardTitle={title}
                    cardDescription={description}
                    reversed={reversed}
                />
                <Divider color="transparent" height={20} />
                <Passages data={passages} />
                {remindButtonStatus ? (
                    <BottomButton
                        label="Nhắc tôi rút bài ngày mai"
                        handler={NotificationHelper.subscribe}
                    />
                ) : (
                    <View />
                )}

                <Modal
                    visible={modalVisible}
                    onRequestClose={() => {}}
                    transparent={true}
                >
                    <ImageViewer
                        onSwipeDown={this.closeModal}
                        enableSwipeDown={true}
                        imageUrls={[{ url: image }]}
                    />
                </Modal>
            </View>
        );
    };

    renderNavBar = () => {
        const { navigate } = this.props.navigation;
        return (
            <NavigationBar
                title="Trang chủ"
                goBack={() => {
                    navigate('Home');
                }}
                actionLabel="Xem Chiêm tinh"
                action={this.gotoAstro}
            />
        );
    };

    renderToolBar = () => {
        const { goBack } = this.props.navigation;
        return <PureCover goBack={goBack} />;
    };

    gotoAstro = () => {
        const { navigate } = this.props.navigation;
        const { profile, astro } = this.props;
        NavigationHelper.gotoAstro(navigate, profile, astro);
    };

    onZoom = () => {
        this.setState({
            modalVisible: true
        });
    };

    share = () => {
        const { tarot, key } = this.props;
        const { title } = this.isHistory()
            ? tarot.data.history[key].card
            : tarot.data.daily.card;
        const { reversed } = this.isHistory()
            ? tarot.data.history[key]
            : tarot.data.daily;

        const image = this.isHistory()
            ? tarot.data.history[key].card.images[80]
            : tarot.data.daily.card.images[80];

        const reversedText = reversed ? 'Ngược' : '';

        const { passage } = this.isHistory()
            ? tarot.data.history[key].article
            : tarot.data.daily.article;

        const passages = TarotHelper.standardlize(passage);
        let caption = title + reversedText;
        for (let k in passages) {
            let p = passages[k];
            caption = caption + '\n' + p.title + '\n\n' + p.content;
        }
        const content = {
            contentType: 'link',
            contentUrl: image,
            contentDescription: caption
        };

        ShareHelper.share(content);
    };

    closeModal = () => {
        this.setState({
            modalVisible: false
        });
    };

    componentDidMount() {
        const { tarot } = this.props;
        console.log('tarot: ', tarot);
        NotificationHelper.checkStatus(notificationPermissionStatus => {
            console.log(notificationPermissionStatus);
            this.setState({
                remindButtonStatus: !notificationPermissionStatus
            });
        });
    }
}

const mapStateToProps = state => ({
    tarot: state.tarot,
    astro: state.astro,
    profile: state.profile,
    facebook: state.facebook
});

const mapDispatchToProps = dispatch => ({
    tarotActions: bindActionCreators(tarotActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(TarotDetailScreen);
