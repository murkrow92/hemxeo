import React from 'react';
import { FlatList, Text, View, StyleSheet } from 'react-native';
import Divider from '../../components/Divider';
import { COLOR_WHITE } from '../../../styles/colors';

class Passages extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                onRefresh={this.refresh}
                refreshing={false}
                ItemSeparatorComponent={() => (
                    <Divider color="transparent" height={10} />
                )}
                data={data}
                renderItem={({ item, index }) => {
                    return <Message item={item} index={index} />;
                }}
            />
        );
    }

    refresh = () => {};
}

const Message = ({ item, index }) => {
    return (
        <View style={styles.messageContainer}>
            <View style={styles.contentContainer}>
                <Text style={styles.title}>{item.title}:</Text>
                <Text style={styles.message}>{item.content}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    messageContainer: {
        flexDirection: 'row'
    },
    title: {
        fontSize: 14,
        fontFamily: 'HelveticaNeue',
        color: COLOR_WHITE,
        fontWeight: 'bold'
    },
    contentContainer: {
        justifyContent: 'center',
        borderRadius: 20,
        paddingHorizontal: 12,
        paddingVertical: 8
    },
    message: {
        marginTop: 10,
        fontSize: 14,
        fontFamily: 'HelveticaNeue',
        color: COLOR_WHITE
    }
});

export default Passages;
