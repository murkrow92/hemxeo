import React from 'react';
import TarotBackground from '../../../assets/images/tarotbg.png';

import { Image, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { ShortButton } from '../../components/Button';
import { COLOR_DESCRIPTION_GRAY, COLOR_WHITE } from '../../../styles/colors';
import { OPEN_SANS } from '../../../styles/fonts';

const CardBox = ({
    cardTitle = '',
    cardDescription = '',
    reversed = false,
    image = 'https://vnastro.com/_img_server/2018/06/26/1529983047_5b31b047c0a1f.png',
    onPressImage,
    onPressShare
}) => {
    const rText = reversed ? 'Ngược' : 'Xuôi';
    const transformDegree = reversed ? '180deg' : '0deg';

    return (
        <View style={styles.container}>
            <View>
                <TouchableOpacity onPress={onPressImage}>
                    <Image
                        resizeMode="contain"
                        source={{ uri: image }}
                        defaultSource={TarotBackground}
                        style={[
                            styles.card,
                            { transform: [{ rotate: transformDegree }] }
                        ]}
                    />
                </TouchableOpacity>
            </View>
            <View style={styles.contentContainer}>
                <Text style={styles.cardTitle}>{cardTitle}</Text>
                <Text style={styles.reversed}>{rText}</Text>
                <View style={{ paddingVertical: 20, width: '50%' }}>
                    <ShortButton label="Chia sẻ" handler={onPressShare} />
                </View>

                {/*<TouchableOpacity>*/}
                {/*<Text*/}
                {/*allowFontScaling={true}*/}
                {/*ellipsizeMode="tail"*/}
                {/*numberOfLines={4}*/}
                {/*style={styles.cardDescription}*/}
                {/*>*/}
                {/*{cardDescription}*/}
                {/*</Text>*/}
                {/*</TouchableOpacity>*/}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        paddingVertical: 10,
        marginTop: 25
    },
    card: {
        height: 137,
        width: 90,
        marginBottom: 10
    },
    contentContainer: {
        flex: 1,
        paddingHorizontal: 15
    },
    cardTitle: {
        color: COLOR_WHITE,
        fontSize: 24,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    },
    reversed: {
        color: COLOR_DESCRIPTION_GRAY,
        fontSize: 16,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    },
    cardDescription: {
        marginTop: 5,
        color: '#D1D1D1',
        fontSize: 14,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    }
});

export default CardBox;
