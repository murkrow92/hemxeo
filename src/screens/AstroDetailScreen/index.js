import React from 'react';
import { View, Platform, Text } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cityActions from '../../redux/actions/CityActions';
import * as astroActions from '../../redux/actions/AstroActions';

import CustomStatusBar from '../../components/CustomStatusBar';
import styleApp from '../../../styles/stylesv3';
import { PureCover } from '../../components/Cover';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import NavigationBar from '../../components/NavigationBar';
import BottomButton from '../../components/BottomButton';
import Conversation from '../../components/Conversation';
import moment from 'moment';
import { COLOR_DESCRIPTION_GRAY } from '../../../styles/colors';
import Divider from '../../components/Divider';
import AstroHelper from '../../helper/AstroHelper';
import AdBanner from '../../components/AdBanner';
import ShareHelper from '../../helper/ShareHelper';
import { VNASTRO_URL } from '../../../config';
import NavigationHelper from '../../helper/NavigationHelper';
import NotificationHelper from '../../helper/NotificationHelper';

class AstroDetailScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            remindButtonStatus: false
        };
    }

    render() {
        return (
            <View style={styleApp.container}>
                <CustomStatusBar backgroundColor="transparent" height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
                <AdBanner />
            </View>
        );
    }

    renderContent = () => {
        const { data } = this.props.astro.data.daily;
        const { remindButtonStatus } = this.state;
        const { main } = this.props.config.data.data.astro;
        const avatar = { uri: main.user.avatar[80] };
        const daily = AstroHelper.getDaily(data, avatar);

        console.log('daily: ', daily);

        const md = moment().format('DD');
        const mm = moment().format('MM');
        const my = moment().format('YYYY');
        return (
            <View style={[styleApp.pageContainer, { marginTop: -100 }]}>
                <Text
                    style={[styleApp.title, { color: COLOR_DESCRIPTION_GRAY }]}
                >
                    Ngày {md} tháng {mm} năm {my}
                </Text>
                <Divider color="transparent" height={20} />
                <Conversation data={daily} />
                {remindButtonStatus ? (
                    <BottomButton
                        label="Nhắc tôi khi có biến cố mới"
                        handler={NotificationHelper.subscribe}
                    />
                ) : (
                    <View />
                )}
            </View>
        );
    };

    renderNavBar = () => {
        const { navigate } = this.props.navigation;
        return (
            <NavigationBar
                title="Trang chủ"
                goBack={() => {
                    navigate('Home');
                }}
                actionLabel="Xem bài Tarot"
                action={this.gotoTarot}
            />
        );
    };

    renderToolBar = () => {
        const { goBack } = this.props.navigation;
        return <PureCover goBack={goBack} />;
    };

    gotoTarot = () => {
        const { tarot } = this.props;
        const { navigate } = this.props.navigation;
        NavigationHelper.gotoTarot(navigate, tarot);
    };

    share = () => {
        const { description, passage } = this.props.astro.data.daily.data;
        let caption = description;
        for (let k in passage) {
            let p = passage[k];
            caption = caption + '\n' + p.title + '\n' + p.content;
        }
        console.log(description, passage);
        const content = {
            contentType: 'link',
            contentUrl: VNASTRO_URL,
            contentDescription: caption
        };

        ShareHelper.share(content);
    };

    componentDidMount() {
        const { astroActions, astro } = this.props;
        const { done } = astro.data.daily;

        if (done == 1) {
            console.log('fetched');
        } else {
            astroActions.daily();
        }

        NotificationHelper.checkStatus(notificationPermissionStatus => {
            console.log(notificationPermissionStatus);
            this.setState({
                remindButtonStatus: !notificationPermissionStatus
            });
        });
    }

    componentDidUpdate(prevProps, prevState) {
        const { astro } = this.props;
        console.log(astro);
    }
}

const mapStateToProps = state => ({
    astro: state.astro,
    tarot: state.tarot,
    facebook: state.facebook,
    city: state.city,
    config: state.config
});

const mapDispatchToProps = dispatch => ({
    astroActions: bindActionCreators(astroActions, dispatch),
    cityActions: bindActionCreators(cityActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AstroDetailScreen);
