import React from 'react';
import { Platform, Text, TouchableOpacity, View } from 'react-native';
import styleApp from '../../../styles/stylesv3';
import CustomStatusBar from '../../components/CustomStatusBar';
import { ProfileCover } from '../../components/Cover';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as profileActions from '../../redux/actions/ProfileActions';
import ImageHelper from '../../helper/ImageHelper';
import CustomDateTimePicker from '../../components/DateTimePicker';
import CityPicker from '../../components/CityPicker';
import CityHelper from '../../helper/CityHelper';
import BottomButton from '../../components/BottomButton';
import Divider from '../../components/Divider';
import CollapsibleToolbar from 'react-native-collapsible-toolbar';
import NavigationBar from '../../components/NavigationBar';
import { COLOR_GREY_99 } from '../../../styles/colors';

class ProfileScreen extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styleApp.container}>
                <CustomStatusBar height={0} />
                <CollapsibleToolbar
                    renderContent={this.renderContent}
                    renderNavBar={this.renderNavBar}
                    renderToolBar={this.renderToolBar}
                    imageSource="../../../assets/images/cover.png"
                    collapsedNavBarBackgroundColor="transparent"
                    translucentStatusBar={Platform.Version >= 21}
                />
            </View>
        );
    }

    renderContent = () => {
        const { city, profile } = this.props;
        const { day, month, year, hour, minute, city_id } = profile.data.data;
        const cityList = CityHelper.getList(city);
        return (
            <View>
                <CustomDateTimePicker
                    ref={picker => (this.datePicker = picker)}
                    day={day}
                    month={month}
                    year={year}
                    hour={hour}
                    minute={minute}
                />
                <CityPicker
                    ref={picker => (this.cityPicker = picker)}
                    cityList={cityList}
                    selectedCityId={city_id}
                />
                <Divider color="transparent" height={30} />
                <BottomButton label="Ghi nhớ" handler={this.handler} />
                <TouchableOpacity
                    onPress={this.gotoLogOut}
                    style={{ alignItems: 'center', justifyContent: 'center' }}
                >
                    <Text
                        style={[
                            styleApp.title,
                            {
                                color: COLOR_GREY_99,
                                textDecorationLine: 'underline'
                            }
                        ]}
                    >
                        Đăng xuất
                    </Text>
                </TouchableOpacity>
            </View>
        );
    };

    renderNavBar = () => {
        const { goBack } = this.props.navigation;
        return <NavigationBar title="Trang chủ" goBack={goBack} />;
    };

    renderToolBar = () => {
        const { facebook } = this.props;
        const { id, picture, name } = facebook.data;
        const avatar = ImageHelper.getPictureFromFacebookObject(id, picture);

        return <ProfileCover fullname={name} avatar={avatar} />;
    };

    handler = () => {
        const { profileActions } = this.props;
        const { data } = this.props.profile.data;
        const { date } = this.datePicker.state;
        const { selectedCityId } = this.cityPicker.state;

        const updated = {
            ...data,
            day: date.getDate(),
            month: date.getMonth() + 1,
            year: date.getFullYear(),
            hour: date.getHours(),
            minute: date.getMinutes(),
            city_id: selectedCityId
        };

        profileActions.update(updated);
    };

    gotoLogOut = () => {
        const { navigate } = this.props.navigation;
        navigate('LogOut');
    };
}

const mapStateToProps = state => ({
    profile: state.profile,
    facebook: state.facebook,
    city: state.city
});

const mapDispatchToProps = dispatch => ({
    profileActions: bindActionCreators(profileActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ProfileScreen);
