import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as cityActions from '../../redux/actions/CityActions';
import * as facebookActions from '../../redux/actions/FacebookActions';
import * as configActions from '../../redux/actions/ConfigActions';
import * as profileActions from '../../redux/actions/ProfileActions';
import * as tarotActions from '../../redux/actions/TarotActions';
import * as astroActions from '../../redux/actions/AstroActions';

import CustomStatusBar from '../../components/CustomStatusBar';
import styleApp from '../../../styles/stylesv3';
import styles from './styles';

class SplashScreen extends React.Component {
    render() {
        const { title } = this.props.config.data.data;
        const background = {
            uri: this.props.config.data.data.loading.background[0]
        };
        const defaultBackground = this.props.config.data.data.loading.background
            .default;

        return (
            <ImageBackground
                defaultSource={defaultBackground}
                style={styleApp.container}
                source={background}
            >
                <CustomStatusBar backgroundColor="transparent" />
                <View style={styles.pageContainer}>
                    <Text style={styleApp.appTitle}>{title}</Text>
                    <Text style={styleApp.title}>Loading ...</Text>
                </View>
            </ImageBackground>
        );
    }

    componentDidMount() {
        const {
            cityActions,
            tarotActions,
            configActions,
            facebookActions,
            astroActions
        } = this.props;
        configActions.fetchConfig();
        cityActions.fetchCity();
        facebookActions.fetchProfile();
        tarotActions.fetchLocalDaily();
        astroActions.fetchLocalDaily();
    }

    componentDidUpdate(prevProps) {
        const prevFetched = prevProps.config.data.fetched;
        const { navigate } = this.props.navigation;
        const { fetched, error } = this.props.config.data;
        const { facebook, profileActions } = this.props;
        const { id, email } = facebook.data;
        const prevId = prevProps.facebook.data.id;

        if (id != prevId) {
            profileActions.signIn(email, id);
        } else if (fetched && !error && !prevFetched) {
            navigate('Home');
        }
    }
}

const mapStateToProps = state => ({
    config: state.config,
    facebook: state.facebook,
    city: state.city,
    profile: state.profile
});

const mapDispatchToProps = dispatch => ({
    cityActions: bindActionCreators(cityActions, dispatch),
    configActions: bindActionCreators(configActions, dispatch),
    facebookActions: bindActionCreators(facebookActions, dispatch),
    profileActions: bindActionCreators(profileActions, dispatch),
    tarotActions: bindActionCreators(tarotActions, dispatch),
    astroActions: bindActionCreators(astroActions, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
