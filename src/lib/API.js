import { API_ENDPOINT } from '../../config';
import lodash from 'lodash';
import queryString from 'query-string';

const RESPONSE_CODE_SUCCESS = 200;

const checkStatus = response => {
    try {
        let json = response.json();
        if (response.status === RESPONSE_CODE_SUCCESS) {
            return json;
        } else {
            console.log(response);
            return json.then(Promise.reject.bind(Promise));
        }
    } catch (e) {
        console.log('error: ', e);
    }
};

const postRequest = (url, body, headers) => {
    console.log('POST url: ', url);
    return fetch(url, { method: 'POST', body, headers })
        .then(checkStatus)
        .catch(error => console.log(error));
};

const getRequest = (url, headers) => {
    console.log('GET url: ', url);
    return fetch(url, { headers }).then(checkStatus);
};
const putRequest = (url, body, headers) =>
    fetch(url, { method: 'PUT', body, headers }).then(checkStatus);

export class API {
    static ACCESS_TOKEN = '';
    static API_ENDPOINT = API_ENDPOINT;

    static header() {
        return new Headers({
            Authorization: API.ACCESS_TOKEN
        });
    }

    static fetchFacebook(facebookToken) {
        const url = `https://graph.facebook.com/me?access_token=${facebookToken}&fields=id,name,picture,friends,email`;
        return getRequest(url);
    }

    static login(email, facebookId) {
        const body = new FormData();
        body.append('email', email);
        body.append('fb_id', facebookId);
        let url = `${this.API_ENDPOINT}/user/fbconnect`;
        return postRequest(url, body);
    }

    static fetchAstro(date, city_id) {
        let c = city_id == 0 ? 22 : city_id;
        let query = queryString.stringify({
            year: date.getFullYear(),
            month: date.getMonth() + 1,
            day: date.getDate(),
            hour: date.getHours(),
            minute: date.getMinutes(),
            city_id: c
        });

        return getRequest(`${this.API_ENDPOINT}/astro?${query}`, API.header());
    }

    static fetchNotifications() {
        return getRequest(`${this.API_ENDPOINT}/notify`, API.header());
    }

    static fetchConversations() {
        let url = `${this.API_ENDPOINT}/conversation`;
        return getRequest(url, API.header());
    }

    static fetchConversation(conversationId) {
        if (conversationId === '0') {
            conversationId = 1;
        }
        let query = queryString.stringify({
            id: conversationId
        });
        let url = `${this.API_ENDPOINT}/conversation?${query}`;
        return getRequest(url, API.header());
    }

    static fetchDetailCombo(combo) {
        const body = new FormData();
        lodash.forEach(combo, function(value, key) {
            body.append(key, value);
        });

        let url = `${this.API_ENDPOINT}/astro/mean`;
        return postRequest(url, body, API.header());
    }

    static fetchFriend() {
        const url = `${this.API_ENDPOINT}/contact`;
        return getRequest(url, API.header());
    }

    static addFriend(friend) {
        const body = new FormData();
        lodash.forEach(friend, function(value, key) {
            body.append(key, value);
        });

        const url = `${this.API_ENDPOINT}/contact/add`;
        return postRequest(url, body, API.header());
    }

    static fetchTransaction(userId) {
        userId = 2;
        let query = queryString.stringify({
            user_id: userId,
            status: 1
        });
        let url = `${this.API_ENDPOINT}/transaction?${query}`;
        return getRequest(url, API.header());
    }

    static saveProfile(profile) {
        const body = new FormData();
        const keys = [
            'id',
            'email',
            'day',
            'month',
            'hour',
            'minute',
            'mobile_phone',
            'year',
            'city_id'
        ];
        lodash.forEach(keys, function(value, key) {
            let field = value;

            if (value === 'id') {
                field = 'user_id';
            }

            if (value == 'mobile_phone') {
                field = 'phone';
            }

            if (profile.hasOwnProperty(value)) {
                body.append(field, profile[value]);
            }
        });

        const url = `${this.API_ENDPOINT}/user/update`;
        return postRequest(url, body, API.header());
    }

    static fetchUserProfile() {
        const url = `${this.API_ENDPOINT}/user`;
        console.log('url:', url, API.ACCESS_TOKEN);
        return getRequest(url, API.header());
    }

    static addConversation(message) {
        const url = `${this.API_ENDPOINT}/conversation/add`;
        const body = new FormData();
        body.append('message', message);
        return postRequest(url, body, API.header());
    }

    static addMessage(conversationId, message) {
        const url = `${this.API_ENDPOINT}/conversation/addMessage`;
        const body = new FormData();
        body.append('conversation_id', conversationId);
        body.append('message', message);
        return postRequest(url, body, API.header());
    }

    static fetchCity() {
        const url = `${this.API_ENDPOINT}/astro/city`;
        const body = new FormData();
        body.append('test', '');
        return postRequest(url, body, API.header());
    }

    static addFacebookFriends(newFriends) {
        const url = `${this.API_ENDPOINT}/contact/addMulti`;
        const body = new FormData();
        body.append('contacts', JSON.stringify(newFriends));
        return postRequest(url, body, API.header());
    }

    static updateFriendProfile(friendProfile) {
        const keys = [
            'contact_id',
            'day',
            'month',
            'hour',
            'minute',
            'year',
            'city_id'
        ];
        const url = `${this.API_ENDPOINT}/contact/update`;
        const body = new FormData();
        lodash.forEach(keys, function(value, key) {
            if (friendProfile.hasOwnProperty(value)) {
                body.append(value, friendProfile[value]);
            }
        });

        return postRequest(url, body, API.header());
    }

    static fetchBase() {
        const url = `${this.API_ENDPOINT}/astro/base`;
        const body = new FormData();
        body.append('test', '');
        return postRequest(url, body, API.header());
    }

    static fetchTimeline() {
        const url = `${this.API_ENDPOINT}/article`;
        return getRequest(url, API.header());
    }

    static fetchReader() {
        const url = `${this.API_ENDPOINT}/reader`;
        return getRequest(url, API.header());
    }

    static fetchEvent(eventParams = {}) {
        let query = queryString.stringify({
            ...eventParams
        });
        const url = `${this.API_ENDPOINT}/event?${query}`;
        return getRequest(url, API.header());
    }

    static fetchProduct() {
        const url = `${this.API_ENDPOINT}/product`;
        return getRequest(url, API.header());
    }

    static fetchFeedback() {
        const url = `${this.API_ENDPOINT}/feedback`;
        return getRequest(url, API.header());
    }

    static book(event_id) {
        const url = `${this.API_ENDPOINT}/event/book`;
        const body = new FormData();
        body.append('event_id', event_id);
        return postRequest(url, body, API.header());
    }

    static fetchBank() {
        const url = `${this.API_ENDPOINT}/event/bank`;
        const body = new FormData();
        body.append('test', '');
        return postRequest(url, body, API.header());
    }

    static fetchArticle(params) {
        let query = queryString.stringify({
            ...params
        });
        const url = `${this.API_ENDPOINT}/article?${query}`;
        return getRequest(url, API.header());
    }

    static fetchConfig() {
        const url = `${this.API_ENDPOINT}/alley/config`;
        return getRequest(url, API.header());
    }

    static dailyTarot() {
        const url = `${this.API_ENDPOINT}/alley/dailyTarot`;
        return getRequest(url, API.header());
    }

    static dailyAstro() {
        const url = `${this.API_ENDPOINT}/alley/dailyAstro`;
        return getRequest(url, API.header());
    }
}
