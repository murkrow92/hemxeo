import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {
    COLOR_DESCRIPTION_GRAY,
    COLOR_LIGHT_BLUE,
    COLOR_WHITE
} from '../../styles/colors';
import { OPEN_SANS } from '../../styles/fonts';
import FontAwesomeIcon from './FontAwesomeIcon';
import DateTimePicker from 'react-native-modal-datetime-picker';
import EvilIcons from './EvilIcons';
import moment from 'moment';

class CustomDateTimePicker extends React.Component {
    constructor(props) {
        super(props);

        const { day, month, year, hour, minute } = this.props;
        let date = new Date();
        if (day > 0 && month > 0 && year > 0) {
            date.setFullYear(year, month - 1, day);
            date.setHours(hour, minute);
        } else {
            date.setFullYear(date.getFullYear() - 24, 0, 1);
        }

        this.state = {
            mode: 'date',
            isVisible: false,
            date: date
        };
    }

    render() {
        const { date } = this.state;

        let md = moment(date).format('DD/MM/YYYY');
        let mt = moment(date).format('HH:mm');

        return (
            <View style={styles.container}>
                <View style={styles.datePicker}>
                    <Text style={styles.title}>Ngày sinh</Text>
                    <TouchableOpacity
                        style={styles.box}
                        onPress={() => {
                            this.showPicker('date');
                        }}
                    >
                        <Text style={styles.label}>{md}</Text>
                        <FontAwesomeIcon
                            name="calendar"
                            size={20}
                            color={COLOR_LIGHT_BLUE}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.timePicker}>
                    <Text style={styles.title}>Giờ sinh</Text>
                    <TouchableOpacity
                        style={styles.box}
                        onPress={() => {
                            this.showPicker('time');
                        }}
                    >
                        <Text style={styles.label}>{mt}</Text>
                        <EvilIcons
                            name="clock"
                            size={30}
                            color={COLOR_LIGHT_BLUE}
                        />
                    </TouchableOpacity>
                </View>
                <DateTimePicker
                    titleStyle={styles.pickerStyle}
                    mode={this.state.mode}
                    datePickerModeAndroid="spinner"
                    titleIOS={
                        this.state.mode === 'date' ? 'Chọn ngày' : 'Chọn giờ'
                    }
                    date={this.state.date}
                    cancelTextIOS="Huỷ bỏ"
                    confirmTextIOS="Chọn"
                    isVisible={this.state.isVisible}
                    onConfirm={this.onSelectDate}
                    onCancel={() => {
                        this.hidePicker();
                    }}
                />
            </View>
        );
    }

    onSelectDate = date => {
        this.setState({ date: date, isVisible: false });
    };

    showPicker = (mode = 'date') => {
        this.setState({ isVisible: true, mode: mode });
    };

    hidePicker = () => {
        this.setState({
            isVisible: false
        });
    };
}

const styles = StyleSheet.create({
    container: {
        marginTop: 20,
        paddingHorizontal: 10,
        flexDirection: 'row'
    },
    datePicker: {
        flex: 2
    },
    timePicker: {
        marginLeft: 15,
        flex: 1.5
    },
    title: {
        color: COLOR_DESCRIPTION_GRAY,
        fontSize: 14,
        fontWeight: '100',
        fontFamily: OPEN_SANS
    },
    box: {
        flexDirection: 'row',
        marginTop: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderColor: COLOR_LIGHT_BLUE,
        borderWidth: 1,
        borderRadius: 5,
        alignItems: 'center'
    },
    label: {
        flex: 1,
        color: COLOR_WHITE,
        fontSize: 18,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    },
    message: {
        fontSize: 14,
        fontFamily: 'HelveticaNeue',
        color: COLOR_WHITE
    }
});

export default CustomDateTimePicker;
