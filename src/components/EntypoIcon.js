import React from 'react';
import Icon from 'react-native-vector-icons/Entypo';

export default (EntypoIcon = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));


