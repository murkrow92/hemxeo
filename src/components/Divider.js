import React from 'react';
import { View } from 'react-native';
import { COLOR_GREY_D7 } from '../../styles/colors';

export default (Divider = ({ color, height }) => (
    <View
        style={{
            width: '100%',
            height: height || 1,
            backgroundColor: color || COLOR_GREY_D7
        }}
    />
));

export const VerticalDivider = ({ color, width }) => (
    <View
        style={{
            height: '100%',
            width: width || 1,
            backgroundColor: color || COLOR_GREY_D7
        }}
    />
);
