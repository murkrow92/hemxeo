import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { COLOR_DARK_33, COLOR_WHITE } from '../../styles/colors';
import PropTypes from 'prop-types';

const Badge = ({ title }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>{title}</Text>
        </View>
    );
};

Badge.propTypes = {
    title: PropTypes.string.isRequired
};

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLOR_DARK_33,
        padding: 2,
        borderRadius: 30
    },
    title: {
        textAlign: 'center',
        fontFamily: 'Arial',
        color: COLOR_WHITE,
        fontSize: 13,
        marginHorizontal: 5
    }
});

export default Badge;
