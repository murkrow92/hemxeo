import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { COLOR_WHITE } from '../../styles/colors';
import Ionicons from './Ionicons';
import { OPEN_SANS } from '../../styles/fonts';
import styleApp from '../../styles/stylesv3';

const NavigationBar = ({
    title,
    goBack,
    actionLabel = '',
    action = () => {}
}) => (
    <View style={styles.bar}>
        <TouchableOpacity
            style={{ flexDirection: 'row', alignItems: 'center' }}
            onPress={() => {
                goBack();
            }}
        >
            <Ionicons name="ios-arrow-back" size={24} color={COLOR_WHITE} />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.actionContainer} onPress={action}>
            <Text style={styleApp.title}>{actionLabel}</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    bar: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    actionContainer: {
        flex: 1,
        alignItems: 'flex-end'
    },
    title: {
        textAlign: 'center',
        marginLeft: 10,
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: '100',
        fontFamily: OPEN_SANS
    }
});

export default NavigationBar;
