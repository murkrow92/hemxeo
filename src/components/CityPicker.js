import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import {
    COLOR_BACKGROUND_BLUE,
    COLOR_DESCRIPTION_GRAY,
    COLOR_LIGHT_BLUE,
    COLOR_WHITE
} from '../../styles/colors';
import { OPEN_SANS } from '../../styles/fonts';
import MaterialIcons from './MaterialIcons';
import PropTypes from 'prop-types';
import CityHelper from '../helper/CityHelper';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';

class CityPicker extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isVisible: false,
            selectedCityId: this.props.selectedCityId
        };
    }

    render() {
        const { cityList } = this.props;
        const { isVisible, selectedCityId } = this.state;
        const selectedCity = CityHelper.getCityById(cityList, selectedCityId);
        const selectedItem = {
            value: selectedCity.id,
            label: selectedCity.title
        };

        return (
            <View style={styles.container}>
                <View style={styles.datePicker}>
                    <Text style={styles.title}>Nơi sinh</Text>
                    <TouchableOpacity
                        style={styles.box}
                        onPress={this.showPicker}
                    >
                        <Text style={styles.label}>{selectedCity.title}</Text>
                        <MaterialIcons
                            name="place"
                            size={20}
                            color={COLOR_LIGHT_BLUE}
                        />
                    </TouchableOpacity>
                </View>
                <SinglePickerMaterialDialog
                    colorAccent={COLOR_BACKGROUND_BLUE}
                    scrolled={true}
                    cancelLabel="Huỷ"
                    okLabel="OK"
                    title="Chọn một tỉnh/thành phố"
                    items={cityList.map((row, index) => {
                        return { value: row.id, label: row.title };
                    })}
                    visible={isVisible}
                    selectedItem={selectedItem}
                    onCancel={this.hidePicker}
                    onOk={this.onSelectedCity}
                />
            </View>
        );
    }

    showPicker = () => {
        this.setState({ isVisible: true });
    };

    hidePicker = () => {
        this.setState({
            isVisible: false
        });
    };

    onSelectedCity = data => {
        this.setState({
            isVisible: false,
            selectedCityId: data.selectedItem.value
        });
    };
}

const styles = StyleSheet.create({
    container: {
        marginTop: 10,
        paddingHorizontal: 10,
        flexDirection: 'row'
    },
    datePicker: {
        flex: 2
    },
    timePicker: {
        marginLeft: 15,
        flex: 1.5
    },
    title: {
        color: COLOR_DESCRIPTION_GRAY,
        fontSize: 14,
        fontWeight: '100',
        fontFamily: OPEN_SANS
    },
    box: {
        flexDirection: 'row',
        marginTop: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        borderColor: COLOR_LIGHT_BLUE,
        borderWidth: 1,
        borderRadius: 5,
        alignItems: 'center'
    },
    label: {
        flex: 1,
        color: COLOR_WHITE,
        fontSize: 18,
        fontWeight: 'normal',
        fontFamily: OPEN_SANS
    },
    message: {
        fontSize: 14,
        fontFamily: 'HelveticaNeue',
        color: COLOR_WHITE
    }
});

CityPicker.propTypes = {
    cityList: PropTypes.array.isRequired,
    selectedCityId: PropTypes.string.isRequired
};

export default CityPicker;
