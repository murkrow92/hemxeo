import React from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';

const EvilIcons = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
);

export default EvilIcons;
