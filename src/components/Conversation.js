import React from 'react';
import { FlatList, Image, Text, View, StyleSheet } from 'react-native';
import Divider from './Divider';
import avatar from '../../assets/images/astro_course.jpg';
import { COLOR_WHITE } from '../../styles/colors';

class Conversation extends React.Component {
    render() {
        const { data } = this.props;
        return (
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                onRefresh={this.refresh}
                refreshing={false}
                ItemSeparatorComponent={() => (
                    <Divider color="transparent" height={2} />
                )}
                data={data}
                renderItem={({ item, index }) => {
                    return <Message item={item} index={index} />;
                }}
            />
        );
    }

    refresh = () => {};
}

const Message = ({ item, index }) => {
    const mT = item.showAvatar && index > 0 ? 20 : 0;
    const bTLR = item.showAvatar ? 20 : 2;
    const bBLR = item.lastChild ? 20 : 2;

    return (
        <View
            style={[
                styles.messageContainer,
                {
                    marginTop: mT
                }
            ]}
        >
            {item.showAvatar ? (
                <Image
                    resizeMode="cover"
                    defaultSource={avatar}
                    source={item.avatar}
                    style={styles.avatar}
                />
            ) : (
                <View style={styles.avatar} />
            )}

            <View
                style={[
                    styles.contentContainer,
                    {
                        borderTopLeftRadius: bTLR,
                        borderBottomLeftRadius: bBLR
                    }
                ]}
            >
                <Text style={styles.message}>{item.content}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    messageContainer: {
        flexDirection: 'row'
    },
    avatar: {
        width: 32,
        height: 32,
        borderRadius: 16
    },
    contentContainer: {
        justifyContent: 'center',
        maxWidth: '75%',
        marginLeft: 10,
        backgroundColor: '#373741',
        borderRadius: 20,
        paddingHorizontal: 12,
        paddingVertical: 8
    },
    message: {
        fontSize: 14,
        fontFamily: 'HelveticaNeue',
        color: COLOR_WHITE
    }
});

export default Conversation;
