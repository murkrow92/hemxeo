import React from 'react';
import PropTypes from 'prop-types';
import { SinglePickerMaterialDialog } from 'react-native-material-dialog';
import { COLOR_BLACK, COLOR_WHITE } from '../../styles/colors';
import CityHelper from '../helper/CityHelper';

export default class OverlayCity extends React.Component {
    render() {
        const {
            isVisible,
            onCancel,
            onOK,
            cityList,
            selectedCityId
        } = this.props;

        const selectedCity = CityHelper.getCityById(cityList, selectedCityId);

        return (
            <SinglePickerMaterialDialog
                scrolled={true}
                backgroundColor={COLOR_BLACK}
                titleColor={COLOR_WHITE}
                cancelLabel="Huỷ"
                okLabel="OK"
                title="Chọn một tỉnh/thành phố"
                colorAccent={COLOR_WHITE}
                items={cityList.map((row, index) => {
                    return { value: row.id.toString(), label: row.title };
                })}
                visible={isVisible}
                selectedItem={{
                    value: selectedCity.id.toString(),
                    label: selectedCity.title
                }}
                onCancel={onCancel}
                onOk={onOK}
            />
        );
    }
}

OverlayCity.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    cityList: PropTypes.array.isRequired,
    onCancel: PropTypes.func.isRequired,
    onOK: PropTypes.func.isRequired,
    selectedCityId: PropTypes.string.isRequired
};

OverlayCity.defaultProps = {
    selectedCityId: 22
};
