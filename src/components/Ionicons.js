import React from 'react';
import Icon from 'react-native-vector-icons/Ionicons';

export default (Ionicons = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));


