import React from 'react';
import Icon from 'react-native-vector-icons/Octicons';

export default (Octicons = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));
