import React from 'react';
import { MaterialDialog } from 'react-native-material-dialog';
import { Text } from 'react-native';
import styleApp from '../../styles/stylesv3';
import { COLOR_BLACK, COLOR_WHITE } from '../../styles/colors';

const Dialog = ({
    title = '',
    description = '',
    isVisible = false,
    onConfirm = () => {},
    onCancel = () => {}
}) => (
    <MaterialDialog
        colorAccent={COLOR_WHITE}
        scrolled={false}
        backgroundColor={COLOR_BLACK}
        titleColor={COLOR_WHITE}
        title={title}
        visible={isVisible}
        onOk={onConfirm}
        onCancel={onCancel}
    >
        <Text style={styleApp.title}>{description}</Text>
    </MaterialDialog>
);

export default Dialog;
