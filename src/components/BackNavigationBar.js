import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { COLOR_BLUE, COLOR_WHITE } from '../../styles/colors';
import PropTypes from 'prop-types';
import { ICON_SIZE_24 } from '../../styles/dimens';
import EntypoIcon from './EntypoIcon';
import MaterialCommunityIcons from './MaterialCommunityIcons';
import FontAwesomeIcon from './FontAwesomeIcon';

const BackNavigationBar = ({ title, goBack }) => (
    <View style={styles.container}>
        <TouchableOpacity
            style={styles.left}
            onPress={() => {
                goBack();
            }}
        >
            <EntypoIcon
                name="chevron-left"
                size={ICON_SIZE_24}
                color={COLOR_WHITE}
            />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
        <View style={styles.right} />
    </View>
);

BackNavigationBar.propTypes = {
    title: PropTypes.string.isRequired,
    goBack: PropTypes.func.isRequired
};

export const ProductBackNavigationBar = ({
    title,
    navigate,
    goBack,
    service
}) => (
    <View style={styles.container}>
        <TouchableOpacity
            onPress={() => {
                goBack();
            }}
            style={styles.left}
        >
            <EntypoIcon
                name="chevron-left"
                size={ICON_SIZE_24}
                color={COLOR_WHITE}
            />
            {service === 'tarot' ? (
                <MaterialCommunityIcons
                    name="cards"
                    size={ICON_SIZE_24}
                    color={COLOR_WHITE}
                />
            ) : (
                <FontAwesomeIcon
                    name="star"
                    size={ICON_SIZE_24}
                    color={COLOR_WHITE}
                />
            )}
            <Text style={[styles.title, { marginLeft: 10 }]}>{title}</Text>
        </TouchableOpacity>
    </View>
);

ProductBackNavigationBar.propTypes = {
    title: PropTypes.string.isRequired,
    goBack: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
    service: PropTypes.string.isRequired
};

export const BackSaveNavigationBar = ({ title, navigate, goBack, onPress }) => (
    <View style={styles.container}>
        <TouchableOpacity
            onPress={() => {
                goBack();
            }}
            style={styles.left}
        >
            <EntypoIcon
                name="chevron-left"
                size={ICON_SIZE_24}
                color={COLOR_WHITE}
            />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.right} onPress={onPress}>
            <Text style={styles.rightTitle}>Lưu lại</Text>
        </TouchableOpacity>
    </View>
);

BackSaveNavigationBar.propTypes = {
    title: PropTypes.string.isRequired,
    goBack: PropTypes.func.isRequired,
    onPress: PropTypes.func.isRequired
};

export const BackConfirmNavigationBar = ({
    title,
    navigate,
    goBack,
    onPress,
    rightText
}) => (
    <View style={styles.container}>
        <TouchableOpacity
            onPress={() => {
                goBack();
            }}
            style={styles.left}
        >
            <EntypoIcon name="chevron-left" size={28} color={COLOR_WHITE} />
            <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.right} onPress={onPress}>
            <Text style={styles.rightTitle}>{rightText}</Text>
        </TouchableOpacity>
    </View>
);

BackConfirmNavigationBar.propTypes = {
    title: PropTypes.string.isRequired,
    goBack: PropTypes.func.isRequired,
    onPress: PropTypes.func.isRequired,
    rightText: PropTypes.string.isRequired
};

export const CompleteNavigationBar = ({ onPress, rightText }) => (
    <View style={styles.container}>
        <TouchableOpacity style={styles.right} onPress={onPress}>
            <Text style={styles.rightTitle}>{rightText}</Text>
        </TouchableOpacity>
    </View>
);

const styles = StyleSheet.create({
    container: {
        paddingVertical: 10,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: COLOR_BLUE,
        alignItems: 'center'
    },
    left: {
        alignItems: 'center',
        flexDirection: 'row',
        paddingHorizontal: 15
    },
    title: {
        paddingRight: 15,
        fontFamily: 'OpenSans',
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: 'bold'
    },
    right: {
        paddingHorizontal: 15,
        flex: 1,
        alignItems: 'flex-end'
    },
    rightTitle: {
        fontFamily: 'OpenSans',
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: 'bold'
    }
});

export default BackNavigationBar;
