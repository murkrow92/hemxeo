import React from 'react';
import { Text } from 'react-native';

export default (BoldText = props => (
    <Text style={{ fontWeight: 'bold' }}>{props.children}</Text>
));
