import React from 'react';
import { View, StyleSheet } from 'react-native';
import { LongButton } from './Button';

const BottomButton = ({ label, handler }) => (
    <View style={styles.bar}>
        <LongButton label={label} handler={handler} />
    </View>
);

const styles = StyleSheet.create({
    bar: {
        marginHorizontal: 15,
        paddingVertical: 15,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'
    }
});

export default BottomButton;
