import { Platform, StyleSheet } from 'react-native';
import { COLOR_DARK_33, COLOR_GREY_F7 } from '../../../styles/colors';

const BORDER_COLOR = COLOR_GREY_F7;
const FONT_SIZE = 13;
const LABEL_FONT_SIZE = 11;
const FONT_WEIGHT = '100';

export default (styleApp = StyleSheet.create({
    container: {
        justifyContent: 'center'
    },
    textbox: {
        textAlignVertical: 'center',
        fontFamily: 'Arial',
        backgroundColor: BORDER_COLOR,
        fontSize: FONT_SIZE,
        height: 32,
        paddingVertical: Platform.OS === 'ios' ? 7 : 0,
        paddingHorizontal: 7,
        borderRadius: 4,
        borderColor: BORDER_COLOR,
        borderWidth: 1,
        marginBottom: 5
    },
    label: {
        color: COLOR_DARK_33,
        fontSize: LABEL_FONT_SIZE,
        fontWeight: FONT_WEIGHT,
        marginBottom: 4
    }
}));
