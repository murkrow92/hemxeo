import React from 'react';
import { Text, View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export const UILabel = ({ label, value, style }) => (
    <View style={[styles.container, { ...style }]}>
        <Text style={styles.label}>{label}</Text>
        <Text underlineColorAndroid="transparent" style={styles.textbox}>
            {value}
        </Text>
    </View>
);

UILabel.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.string
};
