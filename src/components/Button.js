import React from 'react';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';
import { COLOR_WHITE, COLORS_GRADIENT_BLUE } from '../../styles/colors';
import styleApp from '../../styles/stylesv3';
import LinearGradient from 'react-native-linear-gradient';
import { OPEN_SANS } from '../../styles/fonts';

const Button = ({ label, handler }) => (
    <TouchableOpacity
        onPress={() => {
            handler();
        }}
    >
        <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={COLORS_GRADIENT_BLUE}
            style={styleApp.buttonContainer}
        >
            <Text style={styleApp.buttonLabel}>{label}</Text>
        </LinearGradient>
    </TouchableOpacity>
);

export const ShortButton = ({ label, handler }) => (
    <TouchableOpacity
        onPress={() => {
            handler();
        }}
    >
        <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={COLORS_GRADIENT_BLUE}
            style={styles.container}
        >
            <Text style={styles.label}>{label}</Text>
        </LinearGradient>
    </TouchableOpacity>
);

export const LongButton = ({ label, handler }) => (
    <TouchableOpacity
        style={{ width: '100%' }}
        onPress={() => {
            handler();
        }}
    >
        <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1, y: 1 }}
            colors={COLORS_GRADIENT_BLUE}
            style={styles.longButtonContainer}
        >
            <Text style={styleApp.buttonLabel}>{label}</Text>
        </LinearGradient>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    container: {
        borderRadius: 20,
        paddingVertical: 6,
        paddingHorizontal: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    longButtonContainer: {
        borderRadius: 20,
        paddingVertical: 10,
        paddingHorizontal: 30,
        justifyContent: 'center',
        alignItems: 'center'
    },
    label: {
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: 'bold',
        fontFamily: OPEN_SANS
    }
});

export default Button;
