import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';

export default (FontAwesomeIcon = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));


