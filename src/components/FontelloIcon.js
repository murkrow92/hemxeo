import React from 'react';
import { createIconSetFromFontello } from 'react-native-vector-icons';
import config from '../../assets/fonts/config';

export default createIconSetFromFontello(config);
