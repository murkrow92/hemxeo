import React from 'react';
import Icon from 'react-native-vector-icons/Feather';

export default (FeatherIcon = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));


