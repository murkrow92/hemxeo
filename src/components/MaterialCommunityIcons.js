import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default (MaterialCommunityIcons = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));
