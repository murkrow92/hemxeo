import React from 'react';
import styleApp from '../../styles/stylesv3';
import {
    ImageBackground,
    Text,
    StyleSheet,
    View,
    Image,
    TouchableOpacity
} from 'react-native';
import cover from '../../assets/images/cover.png';
import { COLOR_WHITE } from '../../styles/colors';
import { OPEN_SANS } from '../../styles/fonts';

const Cover = ({
    title = 'Hẻm không mua thì Xéo',
    subTitle = 'Dẫm nát mọi rắc rối',
    avatar,
    onPressAvatar = () => {}
}) => {
    return (
        <ImageBackground source={cover} style={styles.cover}>
            <View style={styles.coverTitle}>
                <Text style={styleApp.appTitle}>{title}</Text>
                <Text style={styleApp.subTitle}>{subTitle}</Text>
            </View>
            {avatar && avatar.uri ? (
                <TouchableOpacity
                    onPress={onPressAvatar}
                    style={styles.avatarContainer}
                >
                    <Image
                        resizeMode="cover"
                        source={avatar}
                        style={styleApp.avatar}
                    />
                </TouchableOpacity>
            ) : (
                <View />
            )}
        </ImageBackground>
    );
};

export const PureCover = () => (
    <ImageBackground source={cover} style={styles.coverCollapsible} />
);

export const ProfileCover = ({ fullname, avatar }) => (
    <ImageBackground
        source={cover}
        style={[styles.cover, { alignItems: 'flex-end' }]}
    >
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image resizeMode="cover" source={avatar} style={styleApp.avatar} />
            <Text
                style={[
                    styleApp.appTitle,
                    {
                        marginLeft: 15
                    }
                ]}
            >
                {fullname}
            </Text>
        </View>
    </ImageBackground>
);

const styles = StyleSheet.create({
    cover: {
        flexDirection: 'row',
        height: 165,
        paddingHorizontal: 15,
        paddingBottom: 20
    },
    coverTitle: {
        justifyContent: 'flex-end'
    },
    coverCollapsible: {
        height: 165,
        paddingHorizontal: 15,
        paddingBottom: 20
    },
    bar: {
        paddingVertical: 10,
        alignItems: 'center',
        flexDirection: 'row'
    },
    avatarContainer: {
        flex: 1,
        paddingTop: 35,
        paddingRight: 15,
        alignItems: 'flex-end'
    },
    title: {
        textAlign: 'center',
        marginLeft: 10,
        color: COLOR_WHITE,
        fontSize: 14,
        fontWeight: '100',
        fontFamily: OPEN_SANS
    }
});

export default Cover;
