import React from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import FontAwesomeIcon from './FontAwesomeIcon';
import { COLOR_WHITE, COLOR_YELLOW } from '../../styles/colors';
import { ICON_SIZE_24 } from '../../styles/dimens';

const StarFill = ({ num }) => {
    const size = ICON_SIZE_24;
    return (
        <View style={styles.container}>
            <View style={styles.starContainer}>
                <FontAwesomeIcon
                    name="star"
                    size={size}
                    color={fillColor(1, num)}
                />
            </View>
            <View style={styles.starContainer}>
                <FontAwesomeIcon
                    name="star"
                    size={size}
                    color={fillColor(2, num)}
                />
            </View>
            <View style={styles.starContainer}>
                <FontAwesomeIcon
                    name="star"
                    size={size}
                    color={fillColor(3, num)}
                />
            </View>
            <View style={styles.starContainer}>
                <FontAwesomeIcon
                    name="star"
                    size={size}
                    color={fillColor(4, num)}
                />
            </View>
            <View style={styles.starContainer}>
                <FontAwesomeIcon
                    name="star"
                    size={size}
                    color={fillColor(5, num)}
                />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row'
    },
    starContainer: {
        paddingHorizontal: 5
    }
});

const fillColor = (position, num) =>
    position > num ? COLOR_WHITE : COLOR_YELLOW;

StarFill.propTypes = {
    num: PropTypes.number.isRequired
};

export default StarFill;
