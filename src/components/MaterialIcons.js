import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default (MaterialIcons = ({ name, size, color }) => (
    <Icon name={name} size={size} color={color} />
));


