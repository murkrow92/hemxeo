import React from 'react';
import { StatusBar, View } from 'react-native';
import { STATUS_BAR_HEIGHT } from '../../styles/dimens';

const CustomStatusBar = ({
    height = STATUS_BAR_HEIGHT,
    backgroundColor = 'transparent'
}) => {
    return (
        <View
            style={{
                height: height,
                backgroundColor: backgroundColor
            }}
        >
            <StatusBar
                backgroundColor="transparent"
                translucent
                barStyle="light-content"
            />
        </View>
    );
};

export default CustomStatusBar;
