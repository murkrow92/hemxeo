import React from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import { COLOR_BLUE, COLOR_WHITE } from '../../styles/colors';
import PropTypes from 'prop-types';
import { ICON_SIZE_24 } from '../../styles/dimens';
import EntypoIcon from './EntypoIcon';

const TopBar = ({ title, right, onPress }) => (
    <View style={styles.container}>
        <TouchableOpacity style={styles.left} />
        <Text style={styles.title}>{title}</Text>
        <TouchableOpacity style={styles.right} onPress={onPress}>
            <Text style={styles.rightContent}>{right}</Text>
        </TouchableOpacity>
    </View>
);

TopBar.propTypes = {
    title: PropTypes.string.isRequired,
    right: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        width: '100%',
        flexDirection: 'row',
        backgroundColor: COLOR_BLUE,
        justifyContent: 'center',
        alignItems: 'center'
    },
    left: {
        width: '25%'
    },
    title: {
        flex: 1,
        textAlign: 'center',
        fontFamily: 'OpenSans-Bold',
        color: COLOR_WHITE,
        fontSize: 14
    },
    right: {
        width: '25%',
        alignItems: 'flex-end'
    },
    rightContent: {
        fontFamily: 'OpenSans',
        color: COLOR_WHITE,
        fontSize: 13
    }
});

export default TopBar;
