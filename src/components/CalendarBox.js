import React from 'react';
import moment from 'moment';
import { Text, View, StyleSheet } from 'react-native';
import { COLOR_BLACK, COLOR_GREY_D7, COLOR_RED } from '../../styles/colors';

const d = moment().format('DD');
const m = moment().format('MM');

const CalendarBox = ({ day = d, month = m }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.month}>Tháng {month}</Text>
            <Text style={styles.day}>{day}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: COLOR_GREY_D7,
        borderRadius: 3,
        borderWidth: 1,
        paddingTop: 7,
        paddingBottom: 7,
        paddingHorizontal: 10
    },
    month: {
        fontFamily: 'OpenSans',
        color: COLOR_RED,
        fontSize: 13
    },
    day: {
        fontFamily: 'Helvetica',
        color: COLOR_BLACK,
        fontSize: 28
    }
});

export default CalendarBox;
