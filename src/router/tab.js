import React from 'react';
import { Platform, View, StyleSheet } from 'react-native';

import HomeScreen from '../screens/HomeScreenv2';

import { ICON_SIZE_24, ICON_SIZE_20 } from '../../styles/dimens';
import {
    COLOR_LIGHT_BLACK,
    COLOR_WHITE,
    COLOR_YELLOW
} from '../../styles/colors';

import FontAwesomeIcon from '../components/FontAwesomeIcon';
import MaterialIcons from '../components/MaterialIcons';
import { TabNavigator } from 'react-navigation';
import ProductScreen from '../screens/ProductScreen';
import NotificationScreen from '../screens/NotificationScreen';
import LibraryScreen from '../screens/LibraryScreen';

const ICON_SIZE = Platform.OS === 'ios' ? ICON_SIZE_24 : ICON_SIZE_20;
const LABEL_SIZE = Platform.OS === 'ios' ? 10 : 10;
const ICON_MARGIN_TOP = Platform.OS === 'ios' ? 0 : 5;

const styles = {
    tabContainer: {
        height: 50,
        backgroundColor: COLOR_LIGHT_BLACK,
        margin: 0,
        padding: 0
    },
    tab: {
        borderColor: 'transparent',
        backgroundColor: COLOR_LIGHT_BLACK
    },
    label: {
        fontSize: LABEL_SIZE,
        fontFamily: 'HelveticaNeue',
        marginBottom: Platform.OS === 'ios' ? 5 : 0
    },
    icon: {
        marginTop: ICON_MARGIN_TOP
    }
};

const Tab = TabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                title: 'Trang chủ',
                tabBarIcon: ({ tintColor }) => (
                    <View style={styles.icon}>
                        <FontAwesomeIcon
                            name="home"
                            size={ICON_SIZE}
                            color={tintColor}
                        />
                    </View>
                )
            }
        },
        Notifications: {
            screen: NotificationScreen,
            navigationOptions: {
                title: 'Thông báo',
                tabBarIcon: ({ tintColor }) => (
                    <View style={styles.icon}>
                        <MaterialIcons
                            name="notifications"
                            size={ICON_SIZE}
                            color={tintColor}
                        />
                    </View>
                )
            }
        },
        Library: {
            screen: LibraryScreen,
            navigationOptions: {
                title: 'Thư viện',
                tabBarIcon: ({ tintColor }) => (
                    <View style={styles.icon}>
                        <FontAwesomeIcon
                            name="book"
                            size={ICON_SIZE}
                            color={tintColor}
                        />
                    </View>
                )
            }
        },
        Setting: {
            screen: ProductScreen,
            navigationOptions: {
                title: 'Bạn',
                tabBarIcon: ({ tintColor }) => (
                    <View style={styles.icon}>
                        <FontAwesomeIcon
                            name="user-o"
                            size={ICON_SIZE}
                            color={tintColor}
                        />
                    </View>
                )
            }
        },
        Setting2: {
            screen: LibraryScreen,
            navigationOptions: {
                title: 'feedback',
                tabBarIcon: ({ tintColor }) => (
                    <View style={styles.icon}>
                        <FontAwesomeIcon
                            name="user-o"
                            size={ICON_SIZE}
                            color={tintColor}
                        />
                    </View>
                )
            }
        },
    },
    {
        tabBarPosition: 'bottom',
        swipeEnabled: true,
        animationEnabled: true,
        lazy: false,
        tabBarOptions: {
            initialRouteName: 'Home',
            backBehavior: 'initialRoute',
            activeTintColor: COLOR_YELLOW,
            inactiveTintColor: COLOR_WHITE,
            indicatorStyle: {
                backgroundColor: 'transparent'
            },
            upperCaseLabel: false,
            swipeEnabled: true,
            showLabel: true,
            showIcon: true,
            tabStyle: styles.tabContainer,
            style: styles.tab,
            labelStyle: styles.label
        }
    }
);

export default Tab;
