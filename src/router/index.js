import React from 'react';
import { StackNavigator } from 'react-navigation';
import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/LoginScreenv2';
import HomeScreen from '../screens/HomeScreenv2';
import MainAstroScreen from '../screens/MainAstroScreen';
import MainTarotScreen from '../screens/MainTarotScreen';
import TarotDetailScreen from '../screens/TarotDetailScreen';
import AstroDetailScreen from '../screens/AstroDetailScreen';
import ProfileScreen from '../screens/ProfileScreenv2';
import HistoryScreen from '../screens/HistoryScreen';
import LogOutScreen from '../screens/LogOutScreen';

const AppStack = StackNavigator(
    {
        Splash: SplashScreen,
        AstroDetail: AstroDetailScreen,
        TarotDetail: TarotDetailScreen,
        MainTarot: MainTarotScreen,
        MainAstro: MainAstroScreen,
        Home: HomeScreen,
        Login: LoginScreen,
        Profile: ProfileScreen,
        History: HistoryScreen,
        LogOut: LogOutScreen
    },
    {
        headerMode: 'none'
    }
);

export default AppStack;
