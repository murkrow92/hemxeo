import { combineReducers } from 'redux';
import {
    ACTION_FETCH_EVENT_SUCCESS,
    ACTION_FETCH_EVENT_FAILED,
    ACTION_FETCH_COMING_EVENT_SUCCESS,
    ACTION_FETCH_COMING_EVENT_FAILED,
    ACTION_BOOK_EVENT_SUCCESS,
    ACTION_FETCH_BANK_SUCCESS,
    ACTION_FETCH_BANK_FAILED
} from '../actions/EventActions';

const eventReducer = (
    state = { refreshing: false, data: {}, coming: {}, booked: false },
    action
) => {
    switch (action.type) {
        case ACTION_FETCH_EVENT_SUCCESS:
            return {
                ...state,
                ...action.response,
                booked: false
            };
        case ACTION_FETCH_EVENT_FAILED:
            return { ...state, booked: false };
        case ACTION_FETCH_COMING_EVENT_SUCCESS:
            return {
                ...state,
                coming: action.response.data,
                booked: false
            };
        case ACTION_FETCH_COMING_EVENT_FAILED:
            return {
                ...state,
                booked: false
            };
        case ACTION_BOOK_EVENT_SUCCESS:
            return {
                ...state,
                booked: {
                    event: action.response.data,
                    order: action.response.order
                }
            };
        case ACTION_FETCH_BANK_SUCCESS:
            return {
                ...state,
                bank: action.response.data
            };
        case ACTION_FETCH_BANK_FAILED:
            return state;
        default:
            return state;
    }
};

export default combineReducers({
    data: eventReducer
});
