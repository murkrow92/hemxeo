import { combineReducers } from 'redux';
import {
    ACTION_DRAW_DAILY_CARD_SUCCESS,
    ACTION_DRAW_DAILY_CARD_FAILED,
    ACTION_FETCH_TAROT_HISTORY
} from '../actions/TarotActions';

const defaultState = {
    refreshing: false,
    data: {},
    daily: {
        done: 0,
        article: {
            title: '',
            description: ''
        },
        card: {
            title: '',
            description: '',
            image:
                'https://vnastro.com/_img_server/2018/06/26/1529983047_5b31b047c0a1f.png',
            images: {
                80: 'https://vnastro.com/_img_server/2018/06/26/1529983047_5b31b047c0a1f.png'
            }
        }
    },
    history: {}
};

const tarotReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTION_DRAW_DAILY_CARD_SUCCESS:
            return {
                ...state,
                daily: action.response
            };
        case ACTION_DRAW_DAILY_CARD_FAILED:
            return state;
        case ACTION_FETCH_TAROT_HISTORY:
            return {
                ...state,
                history: action.history
            };
        default:
            return state;
    }
};

export default combineReducers({
    data: tarotReducer
});
