import { combineReducers } from 'redux';
import {
    ACTION_FETCH_FEEDBACK_SUCCESS,
    ACTION_FETCH_FEEDBACK_FAILED
} from '../actions/FeedbackActions';

const feedbackReducer = (
    state = {
        data: {}
    },
    action
) => {
    switch (action.type) {
        case ACTION_FETCH_FEEDBACK_SUCCESS:
            return {
                ...state,
                ...action.response
            };
        case ACTION_FETCH_FEEDBACK_FAILED:
            return {
                ...state
            };

        default:
            return state;
    }
};

export default combineReducers({
    data: feedbackReducer
});
