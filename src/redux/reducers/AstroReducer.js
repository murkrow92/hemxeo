import { combineReducers } from 'redux';
import {
    ACTION_DAILY_FAILED,
    ACTION_DAILY_SUCCESS,
    FETCH_ASTRO_FAILED,
    FETCH_ASTRO_SUCCESS,
    REFRESH_LIST_ASTRO
} from '../actions/AstroActions';

const defaultState = {
    refreshing: false,
    data: {},
    daily: { data: { description: '', passage: [] }, done: 0 }
};

const astroReducer = (state = defaultState, action) => {
    switch (action.type) {
        case FETCH_ASTRO_SUCCESS:
            return {
                ...state,
                ...action.response,
                refreshing: false
            };
        case FETCH_ASTRO_FAILED:
            return {
                ...state,
                ...action.error,
                refreshing: false
            };
        case REFRESH_LIST_ASTRO:
            return {
                ...state,
                refreshing: true
            };
        case ACTION_DAILY_SUCCESS:
            return {
                ...state,
                daily: action.response
            };
        case ACTION_DAILY_FAILED:
            return state;
        default:
            return state;
    }
};

export default combineReducers({
    data: astroReducer
});
