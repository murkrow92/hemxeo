import {
    ACTION_ADD_CONTACT_FAILED,
    ACTION_ADD_CONTACT_SUCCESS,
    FETCH_FRIEND_FAILED,
    FETCH_FRIEND_SUCCESS,
    REFRESH_LIST_FRIEND,
    SEARCH_FRIEND,
    SYNC_FACEBOOK_FAILED,
    SYNC_FACEBOOK_SUCCESS
} from '../actions/FriendActions';
import { combineReducers } from 'redux';

const initialState = {
    data: {},
    refreshing: false,
    facebookSynchronized: true,
    searchText: ''
};

const friendReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_FRIEND_SUCCESS:
            return {
                ...state,
                ...action.friends,
                refreshing: false,
                facebookSynchronized: false
            };
        case FETCH_FRIEND_FAILED:
            return {
                ...state,
                refreshing: false,
                facebookSynchronized: false
            };
        case REFRESH_LIST_FRIEND:
            return {
                ...state,
                refreshing: true,
                facebookSynchronized: false
            };
        case SYNC_FACEBOOK_SUCCESS:
            return {
                ...state,
                refreshing: true,
                facebookSynchronized: true
            };
        case SYNC_FACEBOOK_FAILED:
            return {
                ...state,
                refreshing: false,
                facebookSynchronized: true
            };
        case SEARCH_FRIEND:
            return {
                ...state,
                refreshing: false,
                searchText: action.text
            };
        // case ACTION_SAVE_FRIEND_PROFILE_SUCCESS:
        //     return {
        //         ...state,
        //         refreshing: true
        //     };
        case ACTION_ADD_CONTACT_SUCCESS: {
            return {
                ...state,
                refreshing: true,
                facebookSynchronized: true
            };
        }
        case ACTION_ADD_CONTACT_FAILED: {
            return {
                ...state,
                refreshing: true,
                facebookSynchronized: true
            };
        }

        default:
            return state;
    }
};

export default combineReducers({
    data: friendReducer
});
