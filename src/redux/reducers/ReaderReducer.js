import { combineReducers } from 'redux';
import {
    ACTION_FETCH_READER_SUCCESS,
    ACTION_FETCH_READER_FAILED
} from '../actions/ReaderActions';

const readerReducer = (state = { refreshing: false, data: {} }, action) => {
    switch (action.type) {
        case ACTION_FETCH_READER_SUCCESS:
            return {
                ...state,
                ...action.response
            };
        case ACTION_FETCH_READER_FAILED:
            return state;

        default:
            return state;
    }
};

export default combineReducers({
    data: readerReducer
});
