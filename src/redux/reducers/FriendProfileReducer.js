import {
    ACTION_FRIEND_FORM_DATA_CHANGE,
    ACTION_INIT_STATE,
    ACTION_SAVE_FRIEND_PROFILE_FAILED,
    ACTION_SAVE_FRIEND_PROFILE_SUCCESS
} from '../actions/FriendProfileActions';
import { combineReducers } from 'redux';

const formState = {
    id: 0,
    day: 0,
    month: 0,
    year: 0,
    hour: 0,
    minute: 0,
    city_id: 0,
    name: '',
    fb_id: '',
    updated: false
};

const friendProfileReducer = (state = formState, action) => {
    switch (action.type) {
        case ACTION_INIT_STATE:
            return {
                ...state,
                day: action.day,
                month: action.month,
                year: action.year,
                hour: action.hour,
                minute: action.minute,
                city_id: action.city_id,
                name: action.name,
                fb_id: action.fb_id,
                id: action.id,
                updated: false
            };
        case ACTION_FRIEND_FORM_DATA_CHANGE:
            return {
                ...state,
                [action.key]: action.value,
                updated: false
            };
        case ACTION_SAVE_FRIEND_PROFILE_SUCCESS:
            return {
                ...state,
                updated: true
            };
        case ACTION_SAVE_FRIEND_PROFILE_FAILED:
            return {
                ...state,
                updated: false
            };

        default:
            return state;
    }
};

export default combineReducers({
    data: friendProfileReducer
});
