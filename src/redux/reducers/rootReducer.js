import { combineReducers } from 'redux';
import facebookReducer from './FacebookReducer';
import profileReducer from './ProfileReducer';
import astroReducer from './AstroReducer';
import friendReducer from './FriendReducer';
import cityReducer from './CityReducer';
import readerReducer from './ReaderReducer';
import eventReducer from './EventReducer';
import productReducer from './ProductReducer';
import feedbackReducer from './FeedbackReducer';
import friendProfileReducer from './FriendProfileReducer';
import articleReducer from './ArticleReducer';
import configReducer from './ConfigReducer';
import tarotReducer from './TarotReducer';

export default combineReducers({
    facebook: facebookReducer,
    profile: profileReducer,
    astro: astroReducer,
    friend: friendReducer,
    city: cityReducer,
    reader: readerReducer,
    event: eventReducer,
    product: productReducer,
    feedback: feedbackReducer,
    friendProfile: friendProfileReducer,
    article: articleReducer,
    config: configReducer,
    tarot: tarotReducer
});
