import {
    ACTION_FETCH_PROFILE_SUCCESS,
    ACTION_FETCH_PROFILE_FAILED,
    ACTION_FORM_DATA_CHANGE,
    ACTION_SAVE_SUCCESS,
    ACTION_SAVE_FAILED,
    ACTION_REFRESH_PROFILE,
    ACTION_PROFILE_LOG_OUT
} from '../actions/ProfileActions';
import { combineReducers } from 'redux';
import moment from 'moment';

const date = moment();

const formState = {
    data: {
        id: -1,
        fullname: '',
        astroname: '',
        day: date.format('DD'),
        month: date.format('MM'),
        year: date.format('YYYY'),
        hour: date.format('hh'),
        minute: date.format('mm'),
        account: 0,
        city_id: 22
    },
    token: '',
    refreshing: false,
    updated: false
};

const profileReducer = (state = formState, action) => {
    switch (action.type) {
        case ACTION_FETCH_PROFILE_SUCCESS:
            return {
                ...state,
                ...action.response,
                refreshing: false,
                updated: false
            };

        case ACTION_FETCH_PROFILE_FAILED:
            return {
                ...state,
                refreshing: false,
                updated: false
            };
        case ACTION_REFRESH_PROFILE:
            return {
                ...state,
                refreshing: true,
                updated: false
            };

        case ACTION_FORM_DATA_CHANGE:
            state.data[action.key] = action.value;
            return {
                ...state,
                updated: false
            };
        case ACTION_SAVE_SUCCESS:
            return {
                ...state,
                ...action.profile.data,
                updated: true
            };
        case ACTION_SAVE_FAILED:
            return {
                ...state,
                updated: false
            };
        case ACTION_PROFILE_LOG_OUT:
            return formState;
        default:
            return state;
    }
};

const save = (state, profile) => {
    return {
        ...state,
        profile: profile
    };
};

export default combineReducers({
    data: profileReducer
});
