import { combineReducers } from 'redux';
import {
    ACTION_FETCH_ARTICLE_SUCCESS,
    ACTION_FETCH_ARTICLE_FAILED,
    ACTION_REFRESH_ARTICLE,
    ACTION_FETCH_PAGE_ARTICLE_SUCCESS,
    ACTION_FETCH_PAGE_ARTICLE_FAILED
} from '../actions/ArticleActions';

const articleReducer = (state = { refreshing: false, data: {} }, action) => {
    switch (action.type) {
        case ACTION_FETCH_ARTICLE_SUCCESS:
            return {
                ...state,
                ...action.response,
                refreshing: false
            };

        case ACTION_FETCH_ARTICLE_FAILED:
            return {
                ...state,
                ...action.error,
                refreshing: false
            };
        case ACTION_REFRESH_ARTICLE:
            return {
                ...state,
                refreshing: true
            };
        case ACTION_FETCH_PAGE_ARTICLE_SUCCESS:
            return {
                ...state,
                refreshing: false,
                page: action.response.data
            };
        case ACTION_FETCH_PAGE_ARTICLE_FAILED:
            return {
                ...state,
                refreshing: false
            };
        default:
            return state;
    }
};

export default combineReducers({
    data: articleReducer
});
