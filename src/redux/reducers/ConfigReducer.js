import { combineReducers } from 'redux';
import {
    ACTION_FETCH_CONFIG_SUCCESS,
    ACTION_FETCH_CONFIG_FAILED,
    ACTION_CONFIRM_DIALOG,
    ACTION_CANCEL_DIALOG
} from '../actions/ConfigActions';
import { COLOR_BACKGROUND_BLUE } from '../../../styles/colors';
import DefaultBackground from '../../../assets/images/background.png';

const defaultConfig = {
    showDialog: false,
    fetched: false,
    data: {
        astro: {
            main: {
                user: {
                    avatar: {
                        80: ''
                    }
                }
            }
        },
        tarot: {
            main: {
                user: {
                    avatar: {
                        80: ''
                    }
                }
            }
        },
        backgroundColor: COLOR_BACKGROUND_BLUE,
        title: 'Hẻm không mua thì Xéo',
        caption: 'Dẫm nát mọi rắc rối',
        loading: {
            background: {
                default: DefaultBackground
            },
            cover: {
                default: DefaultBackground
            }
        }
    }
};

const configReducer = (state = defaultConfig, action) => {
    switch (action.type) {
        case ACTION_FETCH_CONFIG_SUCCESS:
            return {
                ...state,
                ...action.response,
                error: false,
                fetched: true
            };
        case ACTION_FETCH_CONFIG_FAILED:
            return {
                ...state,
                fetched: true,
                error: true
            };
        case ACTION_CONFIRM_DIALOG:
            return {
                ...state,
                error: false,
                fetched: true
            };
        case ACTION_CANCEL_DIALOG:
            return {
                ...state,
                error: true,
                fetch: true
            };

        default:
            return state;
    }
};

export default combineReducers({
    data: configReducer
});
