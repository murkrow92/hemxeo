import { combineReducers } from 'redux';
import {
    ACTION_FETCH_PRODUCT_SUCCESS,
    ACTION_FETCH_PRODUCT_FAILED
} from '../actions/ProductActions';

const productReducer = (state = { refreshing: false, data: {} }, action) => {
    switch (action.type) {
        case ACTION_FETCH_PRODUCT_SUCCESS:
            return {
                ...state,
                ...action.response
            };
        case ACTION_FETCH_PRODUCT_FAILED:
            return state;

        default:
            return state;
    }
};

export default combineReducers({
    data: productReducer
});
