import { combineReducers } from 'redux';
import {
    ACTION_FETCH_PROFILE_FACEBOOK_FAILED,
    ACTION_FETCH_PROFILE_FACEBOOK_SUCCESS,
    ACTION_FACEBOOK_LOG_OUT
} from '../actions/FacebookActions';

const initialState = {
    email: '',
    id: '',
    picture: {
        data: {
            url: ''
        }
    },
    name: ''
};

const facebookReducer = (state = initialState, action) => {
    switch (action.type) {
        case ACTION_FETCH_PROFILE_FACEBOOK_SUCCESS:
            return {
                ...state,
                ...action.facebook
            };
        case ACTION_FETCH_PROFILE_FACEBOOK_FAILED:
            return {
                ...state
            };
        case ACTION_FACEBOOK_LOG_OUT:
            return initialState;
        default:
            return state;
    }
};

export default combineReducers({
    data: facebookReducer
});
