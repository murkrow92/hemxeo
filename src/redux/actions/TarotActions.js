import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import moment from 'moment';

export const ACTION_DRAW_DAILY_CARD_SUCCESS =
    'com.action.draw.daily.tarot.success';
export const ACTION_DRAW_DAILY_CARD_FAILED =
    'com.action.draw.daily.tarot.failed';

export const ACTION_FETCH_TAROT_HISTORY = 'com.action.tarot.history.success';

export const drawDailyCard = () => (dispatch, getState) => {
    API.dailyTarot().then(response => {
        const formatter = moment().format('DD/MM/YYYY');
        if (response.done == 1) {
            AsyncStorageHelper.saveTarot(response, formatter);
            dispatch(onDrawDailySuccess(response));
        } else {
            dispatch(fetchLocalDaily());
        }
    });
};

export const fetchLocalDaily = () => (dispatch, getState) => {
    const formatter = moment().format('DD/MM/YYYY');
    AsyncStorageHelper.getTarot(formatter).then(result => {
        if (result != null) {
            const decoded = JSON.parse(result);
            if (decoded.done == 1) {
                dispatch(onDrawDailySuccess(decoded));
            } else {
                dispatch(onDrawDailyFailed());
            }
        } else {
            dispatch(onDrawDailyFailed());
        }
    });
};

const onDrawDailySuccess = response => ({
    type: ACTION_DRAW_DAILY_CARD_SUCCESS,
    response
});

const onDrawDailyFailed = () => ({
    type: ACTION_DRAW_DAILY_CARD_FAILED
});

const getKey = offset => {
    const now = new Date();
    const date = new Date();
    date.setDate(now.getDate() - offset);
    return moment(date).format('DD/MM/YYYY');
};

export const fetchHistory = () => (dispatch, getState) => {
    let fetcher = [];
    for (let i = 0; i < 6; i++) {
        const key = getKey(i);
        fetcher.push(AsyncStorageHelper.getTarot(key));
    }
    Promise.all(fetcher).then(fetchResult => {
        let result = {};
        if (fetchResult != null) {
            for (let i = 0; i < fetchResult.length; i++) {
                const key = getKey(i);
                if (fetchResult[i] != null) {
                    result[key] = JSON.parse(fetchResult[i]);
                }
            }
        }
        dispatch(onFetchHistorySuccess(result));
    });
};

const onFetchHistorySuccess = history => ({
    type: ACTION_FETCH_TAROT_HISTORY,
    history
});
