import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';

export const ACTION_FETCH_READER_SUCCESS = 'com.action.fetch.reader.success';
export const ACTION_FETCH_READER_FAILED = 'com.action.fetch.reader.failed';

export const fetchReader = () => (dispatch, getState) => {
    API.fetchReader().then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveReader(response);
            dispatch(onFetchReaderSuccess(response));
        } else {
            AsyncStorageHelper.getReader().then(result => {
                if (result != null) {
                    dispatch(onFetchReaderSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchReaderFailed());
                }
            });
        }
    });
};

const onFetchReaderSuccess = response => ({
    type: ACTION_FETCH_READER_SUCCESS,
    response
});

const onFetchReaderFailed = () => ({
    type: ACTION_FETCH_READER_FAILED
});
