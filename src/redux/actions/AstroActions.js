import moment from 'moment';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import { API } from '../../lib/API';

export const FETCH_ASTRO_SUCCESS = 'action.fetch.astro.success';
export const FETCH_ASTRO_FAILED = 'action.fetch.astro.failed';
export const REFRESH_LIST_ASTRO = 'action.refresh.list.astro';
export const ACTION_DAILY_SUCCESS = 'action.astro.daily.success';
export const ACTION_DAILY_FAILED = 'action.astro.daily.failed';

let lastFetch = 0;

export const fetchAstro = (date, city_id) => (dispatch, getState) => {
    const key = moment(date).format('MMMM Do YY, h:mm') + city_id;
    const timeInterval = (new Date().getTime() - lastFetch) / 1000;
    if (timeInterval < 3) {
        AsyncStorageHelper.getAstro(key).then(result => {
            if (result != null) {
                dispatch(onFetchAstroSuccess(JSON.parse(result)));
            } else {
                doFetchAstro(dispatch, getState, key, date, city_id);
            }
        });
    } else {
        doFetchAstro(dispatch, getState, key, date, city_id);
    }
};

const doFetchAstro = (dispatch, getState, key, date, city_id) => {
    API.fetchAstro(date, city_id)
        .then(response => {
            if (response.done === 1) {
                AsyncStorageHelper.saveAstro(response, key);
                dispatch(onFetchAstroSuccess(response));
                lastFetch = new Date().getTime();
            } else {
                dispatch(onFetchAstroFailed());
            }
        })
        .catch(error => dispatch(onFetchAstroFailed(error)));
};

const onFetchAstroSuccess = response => ({
    type: FETCH_ASTRO_SUCCESS,
    response
});

const onFetchAstroFailed = error => ({
    type: FETCH_ASTRO_FAILED,
    error
});

export const refresh = (date, city_id) => {
    const key = moment(date).format('MMMM Do YY, h:mm') + city_id;
    AsyncStorageHelper.removeAstro(key);
    return {
        type: REFRESH_LIST_ASTRO
    };
};

export const daily = () => (dispatch, getState) => {
    API.dailyAstro().then(response => {
        const formatter = moment().format('DD/MM/YYYY');
        if (response.done == 1) {
            AsyncStorageHelper.saveDailyAstro(response, formatter);
            dispatch(onDailySuccess(response));
        } else {
            dispatch(fetchLocalDaily());
        }
    });
};

export const fetchLocalDaily = () => (dispatch, getState) => {
    const formatter = moment().format('DD/MM/YYYY');
    AsyncStorageHelper.getDailyAstro(formatter).then(result => {
        console.log('result: ', result);
        if (result != null) {
            const decoded = JSON.parse(result);
            if (decoded.done == 1) {
                dispatch(onDailySuccess(decoded));
            } else {
                dispatch(onDailyFailed());
            }
        } else {
            dispatch(onDailyFailed());
        }
    });
};

const onDailySuccess = response => ({
    type: ACTION_DAILY_SUCCESS,
    response
});

const onDailyFailed = () => ({
    type: ACTION_DAILY_FAILED
});
