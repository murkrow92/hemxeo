import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import { API } from '../../lib/API';

export const ACTION_FETCH_CONFIG_SUCCESS = 'action.fetch.config.success';
export const ACTION_FETCH_CONFIG_FAILED = 'action.fetch.config.failed';
export const ACTION_CONFIRM_DIALOG = 'action.fetch.confirm.dialog';
export const ACTION_CANCEL_DIALOG = 'action.fetch.cancel.dialog';

export const fetchConfig = () => (dispatch, getState) => {
    API.fetchConfig()
        .then(response => {
            if (response.done == 1) {
                AsyncStorageHelper.saveConfig(response);
                dispatch(onFetchConfigSuccess(response));
            } else {
                AsyncStorageHelper.getConfig().then(result => {
                    if (result != null && result.done == 1) {
                        dispatch(onFetchConfigSuccess(JSON.parse(result)));
                    } else {
                        dispatch(onFetchConfigFailed());
                    }
                });
            }
        })
        .catch(error => dispatch(onFetchConfigFailed(error)));
};

const onFetchConfigSuccess = response => ({
    type: ACTION_FETCH_CONFIG_SUCCESS,
    response
});

const onFetchConfigFailed = error => ({
    type: ACTION_FETCH_CONFIG_FAILED,
    error
});

export const confirm = (dispatch, getState) => ({
    type: ACTION_CONFIRM_DIALOG
});

export const cancel = (dispatch, getState) => ({
    type: ACTION_CANCEL_DIALOG
});
