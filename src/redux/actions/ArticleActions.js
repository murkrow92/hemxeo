import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import { REFRESH_LIST_FRIEND } from './FriendActions';

export const ACTION_FETCH_ARTICLE_SUCCESS = 'com.action.fetch.article.success';
export const ACTION_FETCH_ARTICLE_FAILED = 'com.action.fetch.article.failed';
export const ACTION_REFRESH_ARTICLE = 'com.action.refresh.article';
export const ACTION_FETCH_PAGE_ARTICLE_SUCCESS =
    'com.action.fetch.page.article.success';
export const ACTION_FETCH_PAGE_ARTICLE_FAILED =
    'com.action.fetch.page.article.failed';

export const fetchArticle = params => (dispatch, getState) => {
    API.fetchArticle(params).then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveArticles(response);
            dispatch(onFetchArticleSuccess(response));
        } else {
            AsyncStorageHelper.getArticles().then(result => {
                if (result != null) {
                    dispatch(onFetchArticleSuccess(JSON.parse(result), params));
                } else {
                    dispatch(onFetchArticleFailed());
                }
            });
        }
    });
};

const onFetchArticleSuccess = response => ({
    type: ACTION_FETCH_ARTICLE_SUCCESS,
    response
});

const onFetchArticleFailed = () => ({
    type: ACTION_FETCH_ARTICLE_FAILED
});

export const refresh = () => ({
    type: REFRESH_LIST_FRIEND
});

export const fetchPageArticle = params => (dispatch, getState) => {
    API.fetchArticle(params).then(response => {
        if (response.done == 1) {
            dispatch(onFetchPageArticleSuccess(response));
        } else {
            dispatch(onFetchPageArticleFailed());
        }
    });
};

const onFetchPageArticleSuccess = response => ({
    type: ACTION_FETCH_PAGE_ARTICLE_SUCCESS,
    response
});

const onFetchPageArticleFailed = () => ({
    type: ACTION_FETCH_PAGE_ARTICLE_FAILED
});
