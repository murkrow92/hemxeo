import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';

export const ACTION_FETCH_PRODUCT_SUCCESS = 'com.action.fetch.product.success';
export const ACTION_FETCH_PRODUCT_FAILED = 'com.action.fetch.product.failed';

export const fetchProduct = () => (dispatch, getState) => {
    API.fetchProduct().then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveProduct(response);
            dispatch(onFetchProductSuccess(response));
        } else {
            AsyncStorageHelper.getProduct().then(result => {
                if (result != null) {
                    dispatch(onFetchProductSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchProductFailed());
                }
            });
        }
    });
};

const onFetchProductSuccess = response => ({
    type: ACTION_FETCH_PRODUCT_SUCCESS,
    response
});

const onFetchProductFailed = () => ({
    type: ACTION_FETCH_PRODUCT_FAILED
});
