import AlertHelper from '../../helper/AlertHelper';
import { API } from '../../lib/API';

export const ACTION_INIT_STATE = 'com.action.init.state';

export const ACTION_SAVE_FRIEND_PROFILE_SUCCESS =
    'com.action.save.friend.profile.success';
export const ACTION_SAVE_FRIEND_PROFILE_FAILED =
    'com.action.save.friend.profile.failed';

export const ACTION_FRIEND_FORM_DATA_CHANGE = 'com.action.form.friend.change';

export const initState = (
    day = 0,
    month = 0,
    year = 0,
    hour = 0,
    minute = 0,
    name = '',
    fb_id = '',
    city_id = 22,
    id = 0
) => ({
    type: ACTION_INIT_STATE,
    day,
    month,
    year,
    hour,
    minute,
    city_id,
    name,
    fb_id,
    id
});

export const updateFriendProfile = friendProfile => (dispatch, getState) => {
    API.updateFriendProfile(friendProfile.data)
        .then(response => {
            if (response.done == 1) {
                AlertHelper.alertUpdated();
                dispatch(saveSuccess(friendProfile));
            } else {
                dispatch(onSaveFailed());
                AlertHelper.alertError();
            }
        })
        .catch(error => {
            console.log(error);
            AlertHelper.alertError();
        });
};

const saveSuccess = friendProfile => ({
    type: ACTION_SAVE_FRIEND_PROFILE_SUCCESS,
    friendProfile
});

const onSaveFailed = () => ({
    type: ACTION_SAVE_FRIEND_PROFILE_FAILED
});

export const onFormChange = (key, value) => ({
    type: ACTION_FRIEND_FORM_DATA_CHANGE,
    key,
    value
});
