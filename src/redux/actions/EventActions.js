import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import AlertHelper from '../../helper/AlertHelper';

export const ACTION_FETCH_EVENT_SUCCESS = 'com.action.fetch.event.success';
export const ACTION_FETCH_EVENT_FAILED = 'com.action.fetch.event.failed';

export const ACTION_FETCH_COMING_EVENT_SUCCESS =
    'com.action.fetch.coming.event.success';
export const ACTION_FETCH_COMING_EVENT_FAILED =
    'com.action.fetch.coming.event.failed';

export const ACTION_BOOK_EVENT_SUCCESS = 'com.action.book.event.success';
export const ACTION_BOOK_EVENT_FAILED = 'com.action.book.event.failed';

export const ACTION_FETCH_BANK_SUCCESS = 'com.action.fetch.bank.success';
export const ACTION_FETCH_BANK_FAILED = 'com.action.fetch.bank.failed';

export const fetchEvent = (eventParams = {}) => (dispatch, getState) => {
    API.fetchEvent(eventParams).then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveEvent(response);
            dispatch(onFetchEventSuccess(response));
        } else {
            AsyncStorageHelper.getEvent().then(result => {
                if (result != null) {
                    dispatch(onFetchEventSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchEventFailed());
                }
            });
        }
    });
};

const onFetchEventSuccess = response => ({
    type: ACTION_FETCH_EVENT_SUCCESS,
    response
});

const onFetchEventFailed = () => ({
    type: ACTION_FETCH_EVENT_FAILED
});

export const fetchUpcomingEvent = user_id => (dispatch, getState) => {
    API.fetchEvent({
        user_id: user_id
    }).then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveEvent(response);
            dispatch(onFetchComingEventSuccess(response));
        } else {
            AsyncStorageHelper.getEvent().then(result => {
                if (result != null) {
                    dispatch(onFetchComingEventSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchComingEventFailed());
                }
            });
        }
    });
};

const onFetchComingEventSuccess = response => ({
    type: ACTION_FETCH_COMING_EVENT_SUCCESS,
    response
});

const onFetchComingEventFailed = () => ({
    type: ACTION_FETCH_COMING_EVENT_FAILED
});

export const book = event_id => (dispatch, getState) => {
    API.book(event_id).then(response => {
        if (response.done == 1) {
            AlertHelper.alertBookSuccess(() => {
                dispatch(onBookEventSuccess(response));
            });
        } else {
            AlertHelper.alertError(() => {
                dispatch(onBookEventFailed());
            }, response.error);
        }
    });
};

const onBookEventSuccess = response => ({
    type: ACTION_BOOK_EVENT_SUCCESS,
    response
});

const onBookEventFailed = () => ({
    type: ACTION_BOOK_EVENT_FAILED
});

export const fetchBank = () => (dispatch, getState) => {
    API.fetchBank().then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveBank(response);
            dispatch(onFetchBankSuccess(response));
        } else {
            AsyncStorageHelper.getBank().then(result => {
                if (result != null) {
                    dispatch(onFetchBankSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchBankFailed());
                }
            });
        }
    });
};

const onFetchBankSuccess = response => ({
    type: ACTION_FETCH_BANK_SUCCESS,
    response
});

const onFetchBankFailed = () => ({
    type: ACTION_FETCH_BANK_FAILED
});
