import { LoginManager, AccessToken } from 'react-native-fbsdk';
import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';

export const ACTION_FETCH_PROFILE_FACEBOOK_SUCCESS =
    'com.action.facebook.fetch.profile.success';
export const ACTION_FETCH_PROFILE_FACEBOOK_FAILED =
    'com.action.facebook.fetch.profile.failed';
export const ACTION_FACEBOOK_LOG_OUT = 'com.action.facebook.log.out';

export const signIn = () => (dispatch, getState) => {
    LoginManager.logInWithReadPermissions(['public_profile', 'email']).then(
        result => {
            console.log('result:', result);
            if (result.isCancelled) {
                dispatch(onFetchFacebookProfileFailed());
            } else {
                dispatch(fetchProfile());
            }
        },
        error => {
            console.log('Login failed with error: ' + error);
            dispatch(onFetchFacebookProfileFailed());
        }
    );
};

export const fetchProfile = () => (dispatch, getState) => {
    AccessToken.getCurrentAccessToken().then(data => {
        if (data != null) {
            const token = data.accessToken;
            API.fetchFacebook(token).then(result => {
                if (result.id) {
                    AsyncStorageHelper.saveFacebookProfile(result);
                    dispatch(onFetchFacebookProfileSuccess(result));
                } else {
                    dispatch(onFetchFacebookProfileFailed());
                }
            });
        }
    });
};

const onFetchFacebookProfileSuccess = facebook => ({
    type: ACTION_FETCH_PROFILE_FACEBOOK_SUCCESS,
    facebook
});

const onFetchFacebookProfileFailed = () => ({
    type: ACTION_FETCH_PROFILE_FACEBOOK_FAILED
});

export const logout = () => (dispatch, getState) => {
    AsyncStorageHelper.dispatch({ type: ACTION_FACEBOOK_LOG_OUT });
};
