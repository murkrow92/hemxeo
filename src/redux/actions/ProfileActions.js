import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';
import AlertHelper from '../../helper/AlertHelper';

export const ACTION_SAVE_SUCCESS = 'com.action.save.success';
export const ACTION_SAVE_FAILED = 'com.action.save.failed';
export const ACTION_FORM_DATA_CHANGE = 'com.action.form.change';
export const ACTION_FETCH_PROFILE_SUCCESS = 'com.action.fetch.profile.success';
export const ACTION_FETCH_PROFILE_FAILED = 'com.action.fetch.profile.failed';
export const ACTION_REFRESH_PROFILE = 'com.action.refresh.profile';
export const ACTION_PROFILE_LOG_OUT = 'com.action.profile.log.out';

export const signIn = (email, facebookId) => (dispatch, getState) => {
    API.login(email, facebookId)
        .then(response => {
            console.log('sign in response: ', response);

            if (response.done == 1) {
                const token = response.access_token;
                API.ACCESS_TOKEN = token;
                AsyncStorageHelper.saveVnastroToken(token);
                dispatch(fetchProfile());
            } else {
                dispatch(onFetchProfileFailed());
            }
        })
        .catch(error => dispatch(onFetchProfileFailed()));
};

export const fetchProfile = () => (dispatch, getState) => {
    API.fetchUserProfile().then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveUserProfile(response);
            dispatch(onFetchProfileSuccess(response));
        } else {
            AsyncStorageHelper.getUserProfile().then(result => {
                if (result != null) {
                    dispatch(onFetchProfileSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchProfileFailed());
                }
            });
        }
    });
};

const onFetchProfileSuccess = response => ({
    type: ACTION_FETCH_PROFILE_SUCCESS,
    response
});

const onFetchProfileFailed = () => ({
    type: ACTION_FETCH_PROFILE_FAILED
});

export const refresh = () => ({
    type: ACTION_REFRESH_PROFILE
});

export const update = profile => (dispatch, getState) => {
    API.saveProfile(profile)
        .then(response => {
            if (response.done == 1) {
                AlertHelper.alertUpdated(() => {
                    dispatch(saveSuccess(profile));
                });
            } else {
                dispatch(saveFailed());
            }
        })
        .catch(error => {
            console.log(error);
            dispatch(saveFailed());
        });
};

const saveSuccess = profile => ({
    type: ACTION_SAVE_SUCCESS,
    profile
});

const saveFailed = () => ({
    type: ACTION_SAVE_FAILED
});

export const onFormChange = (key, value) => ({
    type: ACTION_FORM_DATA_CHANGE,
    key,
    value
});

export const logout = () => (dispatch, getState) => {
    AsyncStorageHelper.removeUserProfile().then(response => {
        dispatch(doLogout());
    });
};

const doLogout = () => ({
    type: ACTION_PROFILE_LOG_OUT
});
