import { API } from '../../lib/API';
import AsyncStorageHelper from '../../helper/AsyncStorageHelper';

export const ACTION_FETCH_FEEDBACK_SUCCESS = 'com.action.fetch.feedback.success';
export const ACTION_FETCH_FEEDBACK_FAILED = 'com.action.fetch.feedback.failed';

export const fetchFeedback = () => (dispatch, getState) => {
    API.fetchFeedback().then(response => {
        if (response.done == 1) {
            AsyncStorageHelper.saveFeedback(response);
            dispatch(onFetchFeedbackSuccess(response));
        } else {
            AsyncStorageHelper.getFeedback().then(result => {
                if (result != null) {
                    dispatch(onFetchFeedbackSuccess(JSON.parse(result)));
                } else {
                    dispatch(onFetchFeedbackFailed());
                }
            });
        }
    });
};

const onFetchFeedbackSuccess = response => ({
    type: ACTION_FETCH_FEEDBACK_SUCCESS,
    response
});

const onFetchFeedbackFailed = () => ({
    type: ACTION_FETCH_FEEDBACK_FAILED
});
