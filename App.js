import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './src/redux/configureStore';
import AppStack from './src/router';
import codePush from 'react-native-code-push';
import NotificationHelper from './src/helper/NotificationHelper';

class App extends React.Component {
    render() {
        return (
            <Provider store={configureStore()}>
                <AppStack />
            </Provider>
        );
    }

    componentWillMount() {
        NotificationHelper.register();
    }

    componentWillUnmount() {
        NotificationHelper.unregister();
    }
}

export default codePush(App);
