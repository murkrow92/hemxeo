import { Platform } from 'react-native';

export const API_ENDPOINT = 'https://api.vnastro.com/1.0';
// export const API_ENDPOINT = 'http://lcapi.vnastro.com/1.0';

export const BANNER_ID =
    Platform.OS === 'ios'
        ? `ca-app-pub-7892858844183742/7844165736`
        : `ca-app-pub-7892858844183742/4274980103`;
export const FANPAGE_ID = '817755621606346';
export const GOOGLE_ANALYTIC_TRACKING_ID = 'UA-115187674-1';

export const VNASTRO_CHART_URL = 'http://vnastro.com/chart.html';

export const INTERSTITIAL_ID = `ca-app-pub-7892858844183742/3615794110`;
export const FACEBOOK_APP_ID = '30572648044';
export const APP_LINK_INVITE_ANDROID = 'https://goo.gl/fBj3jr';
export const APP_LINK_INVITE_IOS = 'https://goo.gl/vppnQf';
export const OZI_URL = 'http://ozidigital.com/';
export const VNASTRO_URL = 'https://vnastro.com';
export const FANPAGE_URL_FOR_BROWSER = `https://fb.com/${FANPAGE_ID}`;
export const FANPAGE_URL_FOR_APP = `fb://page/${FANPAGE_ID}`;
export const WEB_TITLE = 'Học viện Chiêm tinh Việt Nam';

export const ONE_SINGNAL_APP_ID = '5a0fad5f-bc33-4095-9a26-6c34a667226a';
